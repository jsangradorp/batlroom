/*
    batlroom
    Copyright (C) 2021  Julio Sangrador-Paton

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

static void
drawLine(
    HiResCoord x1, HiResCoord y1, HiResCoord x2, HiResCoord y2, Color color)
{
  SAF_drawLine(LO_RES(x1), LO_RES(y1), LO_RES(x2), LO_RES(y2), color);
}

static void
drawCircle(
    HiResCoord x,
    HiResCoord y,
    HiResCoord radius,
    Color color,
    int is_filled)
{
  SAF_drawCircle(LO_RES(x), LO_RES(y), LO_RES(radius), color, is_filled);
}

static void
drawStrokedFilledCircle(
    HiResCoord x,
    HiResCoord y,
    HiResCoord radius,
    Color color,
    Color stroke_color) {
  drawCircle(x, y, radius, color, FILLED);
  drawCircle(x, y, radius, stroke_color, NOTFILLED);
}

static void
drawTextWithBorder(
    const char *text,
    uint8_t x,
    uint8_t y,
    Color color,
    Color shadow_color,
    uint8_t size)
{
  SAF_drawText(text, x-1, y-1, shadow_color, size);
  SAF_drawText(text, x-1, y+0, shadow_color, size);
  SAF_drawText(text, x-1, y+1, shadow_color, size);
  SAF_drawText(text, x+0, y-1, shadow_color, size);
  SAF_drawText(text, x+0, y+1, shadow_color, size);
  SAF_drawText(text, x+1, y-1, shadow_color, size);
  SAF_drawText(text, x+1, y+0, shadow_color, size);
  SAF_drawText(text, x+1, y+1, shadow_color, size);
  SAF_drawText(text, x, y, color, size);
}

static void
centerTextWithBorder(
    uint8_t line,
    const char *text,
    Color color,
    Color shadow_color,
    uint8_t size)
{
  uint8_t length = 0;
  const char *cursor = text;
  while (*cursor++)
    length++;
  uint8_t y = line * 6;
  uint8_t x = (63 - (length * 6 - 2) * size) / 2 + 1;
  drawTextWithBorder(text, x, y, color, shadow_color, size);
}

static LoResValue
sine(Angle angle)
{
  return SAF_sin(LO_RES_ANGLE(angle));
}

static LoResValue
cosine(Angle angle)
{
  return SAF_cos(LO_RES_ANGLE(angle));
}

static const LoResValue SIN_RESOLUTION = 127;

static HiResValue
sine_projection(HiResValue magnitude, Angle angle)
{
  return magnitude / SIN_RESOLUTION * sine(angle);
}

static HiResValue
cosine_projection(HiResValue magnitude, Angle angle)
{
  return magnitude / SIN_RESOLUTION * cosine(angle);
}

static Angle
sum_angles(Angle a1, Angle a2)
{
  return (a1 + a2) & 0x7fffffff;
}

static Angle
subtract_angles(Angle minuend, Angle subtrahend)
{
  return sum_angles(minuend, TWOPI-subtrahend);
}

static Angle
negated_angle(Angle angle)
{
  return subtract_angles(TWOPI, angle);
}

static Angle
angle_distance(Angle a1, Angle a2)
{
  Angle d1 = subtract_angles(a1, a2);
  Angle d2 = subtract_angles(a2, a1);
  return d1 < d2 ? d1 : d2;
}

static uint8_t
anyButtonPressed()
{
  return SAF_buttonJustPressed(SAF_BUTTON_A) ||
    SAF_buttonJustPressed(SAF_BUTTON_B) ||
    SAF_buttonJustPressed(SAF_BUTTON_C);
}
