/*
    batlroom
    Copyright (C) 2021  Julio Sangrador-Paton

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

typedef enum {
  STANDARD_ROOM,
  LARGE_ROOM,
} RoomSize;

typedef enum {
  DARK,
  LIGHT,
} Lighting;

#ifdef BUILD_LEVEL_EDITOR

static char *room_sizes[2] = {
  "STANDARD_ROOM",
  "LARGE_ROOM"
};

static char *lighting_types[2] = {
  "DARK",
  "LIGHT",
};

static char *orientations[8] = {
  "NORTH",
  "NORTHEAST",
  "EAST",
  "SOUTHEAST",
  "SOUTH",
  "SOUTHWEST",
  "WEST",
  "NORTHWEST"
};

#endif

typedef struct {
  Coord x;
  Coord y;
} Star;

#define STAR_SIZE (6)
#define MAX_STARS_PER_ROOM (5)
typedef struct {
  RoomSize room_size:1;
  Lighting lighting:1;
  uint8_t n_opponents:3;
  uint8_t n_stars:3;
  Star stars[MAX_STARS_PER_ROOM];
  Doorway start_doorway;
  Doorway goal_doorway;
#ifndef SMALLER_RAM
  uint8_t time;
#endif
} LevelData;

#define MAX_LEVELS 255
#define MAX_OPPONENTS 6
#include "level_data.h"
