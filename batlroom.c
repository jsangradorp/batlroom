/*
    batlroom
    Copyright (C) 2021  Julio Sangrador-Paton

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#define SAF_PROGRAM_NAME "Battle Room"
#define SAF_SETTING_1BIT_DITHER 1

#ifdef SAF_PLATFORM_ARDUBOY
#define SMALLER_RAM
#endif
#ifdef SMALLER_RAM
#undef BUILD_LEVEL_EDITOR
#endif

#include "lib/saf-jsp.h"

typedef uint32_t UnsignedHiResValue;
typedef int32_t HiResValue;
typedef uint8_t UnsignedLoResValue;
typedef int8_t LoResValue;
typedef uint8_t Byte;

typedef LoResValue Coord;
typedef UnsignedHiResValue Angle;
typedef UnsignedLoResValue LoResAngle;
typedef LoResValue Magnitude;
typedef HiResValue HiResCoord;
typedef Byte Color;
typedef Byte *Image;
typedef Byte Transform;

#undef sgn
#undef abs
#define sgn(n) (((n) > 0) - ((n) < 0))
#define abs(n) (sgn(n)*(n))

static const int rotval = ((sizeof(HiResValue) / sizeof(LoResValue)) - 1) * 8;
#define LO_RES(x) ((x) >> rotval)
#define HI_RES(x) (((HiResValue)(x)) << rotval)
static const int rotval_angles = rotval - 1;
#define LO_RES_ANGLE(x) (x >> rotval_angles)
#define HI_RES_ANGLE(x) (((Angle)x) << rotval_angles)
#undef PI
#define PI ((Angle)0x40000000)
#define TWOPI ((Angle)0x80000000)

#define FILLED 1
#define NOTFILLED 0

#include "saf_adapters.h"

#define BATTLEROOM_WIDTH (HI_RES(64))
#define BATTLEROOM_HEIGHT (HI_RES(64))
#define CENTER_X (BATTLEROOM_WIDTH / 2)
#define CENTER_Y (BATTLEROOM_HEIGHT / 2)

#define CLOSEUP_HELMET_RADIUS (BATTLEROOM_WIDTH / 8)
#define CLOSEUP_HAND_RADIUS (CLOSEUP_HELMET_RADIUS / 4 * 1)
#define CLOSEUP_ELBOW_RADIUS (CLOSEUP_HELMET_RADIUS / 4 * 2)
#define CLOSEUP_SHOULDER_RADIUS (CLOSEUP_HELMET_RADIUS / 4 * 3)
#define CLOSEUP_ARM_LENGTH (CLOSEUP_SHOULDER_RADIUS + CLOSEUP_ELBOW_RADIUS)
#define CLOSEUP_FOREARM_LENGTH (CLOSEUP_ELBOW_RADIUS + CLOSEUP_HAND_RADIUS)
#define CLOSEUP_COLLARBONE_LENGTH (CLOSEUP_HELMET_RADIUS / 5 * 3)
#ifndef SMALLER_RAM
#define PISTOL_HALFLENGTH (CLOSEUP_HELMET_RADIUS)
#endif
#define CLOSEUP_VISOR_RADIUS (CLOSEUP_HELMET_RADIUS / 4 * 3)

#define FARVIEW_HELMET_RADIUS (HI_RES(2))
#define FARVIEW_VISOR_RADIUS (FARVIEW_HELMET_RADIUS / 4 * 3)

#define MAX_ROTATION_SPEED (HI_RES_ANGLE(10))
#define MIN_THRUST 50
#define MAX_THRUST 100
#define THRUST_DELTA 7

#define TURNING_SPEED (16)
#define MIN_LOOK_ANGLE (PI / 8 * 13)
#define MAX_LOOK_ANGLE (PI / 8 * 3)
#define DEFAULT_LOOK_ANGLE (0)
#define LOOK_TURNING_SPEED (16)

#define MAX_POSSIBLE_SCORE (9999999)
#define FRAMES_TO_SURRENDER (SAF_FPS * 5)
#define FRAMES_TO_FROZEN 3

#if (SAF_PLATFORM_COLOR_COUNT > 2)
/* LIGHT THEME */
  #define LT_WALL_COLOR (SAF_COLOR_BLACK)
  #define LT_BACKGROUND_COLOR (SAF_COLOR_WHITE)
  #define LT_TEXT_COLOR (SAF_COLOR_BLACK)
  #define LT_FARVIEW_STROKE_COLOR (SAF_COLOR_BLACK)
  #define LT_PLAYER_HELMET_COLOR (SAF_COLOR_YELLOW)
  #define LT_OPPONENT_HELMET_COLOR (SAF_COLOR_WHITE)
  #define LT_SHINE_COLOR (SAF_COLOR_WHITE)
  #define LT_CLOSEUP_STROKE_COLOR (SAF_COLOR_GRAY)
  #define LT_VISOR_COLOR (SAF_COLOR_GRAY)
  #define LT_LASER_COLOR1 (SAF_COLOR_GRAY)
  #define LT_LASER_COLOR2 (SAF_COLOR_GRAY_DARK)
  #define LT_FROZEN_HELMET_COLOR (SAF_COLOR_RGB(0, 255, 255))
  #define LT_FROZEN_STROKE_COLOR (SAF_COLOR_BLUE)
  #define LT_HIT_HELMET_COLOR (SAF_COLOR_WHITE)
  #define LT_HIT_STROKE_COLOR (SAF_COLOR_GRAY)
/* DARK THEME */
  #define DT_WALL_COLOR (SAF_COLOR_GRAY_DARK)
  #define DT_BACKGROUND_COLOR (SAF_COLOR_BLACK)
  #define DT_TEXT_COLOR (SAF_COLOR_WHITE)
  #define DT_FARVIEW_STROKE_COLOR (SAF_COLOR_GRAY_DARK)
  #define DT_PLAYER_HELMET_COLOR (SAF_COLOR_BROWN)
  #define DT_OPPONENT_HELMET_COLOR (SAF_COLOR_BLACK)
  #define DT_SHINE_COLOR (SAF_COLOR_BLACK)
  #define DT_CLOSEUP_STROKE_COLOR (SAF_COLOR_GRAY_DARK)
  #define DT_VISOR_COLOR (SAF_COLOR_GRAY_DARK)
  #define DT_LASER_COLOR1 (SAF_COLOR_GRAY)
  #define DT_LASER_COLOR2 (SAF_COLOR_GRAY_DARK)
  #define DT_FROZEN_HELMET_COLOR (SAF_COLOR_RGB(0, 255, 255))
  #define DT_FROZEN_STROKE_COLOR (SAF_COLOR_BLUE)
  #define DT_HIT_HELMET_COLOR (SAF_COLOR_WHITE)
  #define DT_HIT_STROKE_COLOR (SAF_COLOR_GRAY)
#else
  #define MONOCHROME
/* LIGHT THEME */
  #define LT_WALL_COLOR (SAF_COLOR_BLACK)
  #define LT_BACKGROUND_COLOR (SAF_COLOR_WHITE)
  #define LT_TEXT_COLOR (SAF_COLOR_BLACK)
  #define LT_FARVIEW_STROKE_COLOR (SAF_COLOR_BLACK)
  #define LT_PLAYER_HELMET_COLOR (SAF_COLOR_GRAY_DARK)
  #define LT_OPPONENT_HELMET_COLOR (SAF_COLOR_WHITE)
  #define LT_SHINE_COLOR (SAF_COLOR_WHITE)
  #define LT_CLOSEUP_STROKE_COLOR (SAF_COLOR_BLACK)
  #define LT_VISOR_COLOR (SAF_COLOR_BLACK)
  #define LT_LASER_COLOR1 (SAF_COLOR_GRAY)
  #define LT_LASER_COLOR2 (SAF_COLOR_GRAY_DARK)
  #define LT_FROZEN_HELMET_COLOR (SAF_COLOR_BLACK)
  #define LT_FROZEN_STROKE_COLOR (SAF_COLOR_BLACK)
  #define LT_HIT_HELMET_COLOR (SAF_COLOR_WHITE)
  #define LT_HIT_STROKE_COLOR (SAF_COLOR_GRAY)
/* DARK THEME */
  #define DT_WALL_COLOR (SAF_COLOR_WHITE)
  #define DT_BACKGROUND_COLOR (SAF_COLOR_BLACK)
  #define DT_TEXT_COLOR (SAF_COLOR_WHITE)
  #define DT_FARVIEW_STROKE_COLOR (SAF_COLOR_WHITE)
  #define DT_PLAYER_HELMET_COLOR (SAF_COLOR_GRAY_DARK)
  #define DT_OPPONENT_HELMET_COLOR (SAF_COLOR_BLACK)
  #define DT_SHINE_COLOR (SAF_COLOR_BLACK)
  #define DT_CLOSEUP_STROKE_COLOR (SAF_COLOR_WHITE)
  #define DT_VISOR_COLOR (SAF_COLOR_WHITE)
  #define DT_LASER_COLOR1 (SAF_COLOR_GRAY)
  #define DT_LASER_COLOR2 (SAF_COLOR_WHITE)
  #define DT_FROZEN_HELMET_COLOR (SAF_COLOR_WHITE)
  #define DT_FROZEN_STROKE_COLOR (SAF_COLOR_WHITE)
  #define DT_HIT_HELMET_COLOR (SAF_COLOR_WHITE)
  #define DT_HIT_STROKE_COLOR (SAF_COLOR_GRAY)
#endif

#define WALL_COLOR theme_color(LT_WALL_COLOR, DT_WALL_COLOR)
#define BACKGROUND_COLOR (theme_color(LT_BACKGROUND_COLOR, \
      DT_BACKGROUND_COLOR))
#define TEXT_COLOR (theme_color(LT_TEXT_COLOR, DT_TEXT_COLOR))
#define FARVIEW_STROKE_COLOR (theme_color(LT_FARVIEW_STROKE_COLOR, \
      DT_FARVIEW_STROKE_COLOR))
#define PLAYER_HELMET_COLOR (theme_color(LT_PLAYER_HELMET_COLOR, \
      DT_PLAYER_HELMET_COLOR))
#define OPPONENT_HELMET_COLOR (theme_color(LT_OPPONENT_HELMET_COLOR, \
      DT_OPPONENT_HELMET_COLOR))
#define SHINE_COLOR (theme_color(LT_SHINE_COLOR, DT_SHINE_COLOR))
#define CLOSEUP_STROKE_COLOR (theme_color(LT_CLOSEUP_STROKE_COLOR, \
      DT_CLOSEUP_STROKE_COLOR))
#define VISOR_COLOR (theme_color(LT_VISOR_COLOR, DT_VISOR_COLOR))
#define LASER_COLOR1 (theme_color(LT_LASER_COLOR1, DT_LASER_COLOR1))
#define LASER_COLOR2 (theme_color(LT_LASER_COLOR2, DT_LASER_COLOR2))
#define FROZEN_HELMET_COLOR (theme_color(LT_FROZEN_HELMET_COLOR, \
      DT_FROZEN_HELMET_COLOR))
#define FROZEN_STROKE_COLOR (theme_color(LT_FROZEN_STROKE_COLOR, \
      DT_FROZEN_STROKE_COLOR))
#define HIT_HELMET_COLOR (theme_color(LT_HIT_HELMET_COLOR, \
      DT_HIT_HELMET_COLOR))
#define HIT_STROKE_COLOR (theme_color(LT_HIT_STROKE_COLOR, \
      DT_HIT_STROKE_COLOR))

#ifdef SMALLER_RAM
#define LEVEL_TIME 120
#endif

typedef struct {
  HiResValue numerator;
  HiResValue denominator;
} Fraction;

#define HALF ((Fraction){.numerator = 1,.denominator = 2,})

static const Fraction WALL_LINEAR_FRICTION_COEF =
  { .numerator =  459, .denominator = 500};
static const Fraction WALL_ANGULAR_FRICTION_COEF =
  { .numerator =  499, .denominator = 500};
static const Fraction AIR_LINEAR_FRICTION_COEF =
  { .numerator =  499, .denominator = 500};
static const Fraction AIR_ANGULAR_FRICTION_COEF =
  { .numerator =  499, .denominator = 500};
static const Fraction BUMP_COEF = { .numerator =  1, .denominator = 2};


static Angle
adjust_angle_by(Fraction coefficient, Angle angle)
{
  return
    angle < PI ?
      angle / coefficient.denominator * coefficient.numerator
      :
      TWOPI -
        ((TWOPI - angle) / coefficient.denominator * coefficient.numerator);
}

static HiResValue
adjust_by(Fraction coefficient, HiResValue value)
{
  return value / coefficient.denominator * coefficient.numerator;
}

typedef enum {
  FLYING,
  ATTEMPTING_TO_CLING,
  CLINGING,
  GAINING_THRUST
} PlayerStatus;

typedef enum {
  PLAYER_INPUT,
  ENTER_ROOM,
  DART_TO_GOAL,
} TrooperAction;

typedef struct {
  HiResCoord x;
  HiResCoord y;
  HiResValue speed;
  Angle direction;
  Angle bearing;
  Angle look;
  Angle base_rotation_speed;
  LoResAngle turning_speed;
  LoResAngle head_turning_speed;
  PlayerStatus status;
  Magnitude thrust;
  Magnitude thrust_delta;
  uint8_t is_shooting;
  uint8_t is_frozen;
  uint8_t is_hit;
  uint8_t frames_hit_until_frozen;
  TrooperAction action;
  Coord point_of_impact_x;
  Coord point_of_impact_y;
} Trooper;

typedef struct {
  uint8_t show_closeup;
  Trooper *trooper;
} Player;

static Player player = {
  .show_closeup = 0,
  .trooper = 0
};

typedef enum {
  NORTH, NORTHEAST, EAST, SOUTHEAST, SOUTH, SOUTHWEST, WEST, NORTHWEST
} WallOrientation;

static Angle wall_normals[8] = {
  PI / 2 * 3, /* NORTH */
  PI / 4 * 7, /* NORTHEAST */
  0, /* EAST */
  PI / 4 * 1, /* SOUTHEAST */
  PI / 2, /* SOUTH */
  PI / 4 * 3, /* SOUTHWEST */
  PI, /* WEST */
  PI / 4 * 5 /* NORTHWEST */
};

typedef struct {
  Coord x1, y1, x2, y2;
  WallOrientation orientation;
} Wall;

typedef Wall Doorway;

typedef enum {
  PLAYING,
  CLEARED,
  TIMED_OUT,
  SURRENDER,
  LOST
} LevelStatus;

#include "levels.h"

typedef struct {
  LevelData *level_data;
  uint16_t level_time;
  uint16_t remaining_frames;
  uint8_t surrender_frames;
  LevelStatus status;
  Trooper troopers[MAX_OPPONENTS+1]; /* [0] is the player's trooper */
  Wall walls[4 + 4 * MAX_STARS_PER_ROOM];
  uint8_t n_walls;
} Level;

static Level level;

static Color
theme_color(Color light_theme_color, Color dark_theme_color)
{
  return
    level.level_data->lighting ?
      light_theme_color
      :
      dark_theme_color;
}

static void
place_trooper_at_doorway(Trooper *trooper, Doorway *doorway)
{
  trooper->x = (HI_RES(doorway->x1) + HI_RES(doorway->x2)) / 2;
  trooper->y = (HI_RES(doorway->y1) + HI_RES(doorway->y2)) / 2;
  trooper->bearing = trooper->direction = wall_normals[doorway->orientation];
}

static void
initialize_level_sides(void)
{
  level.walls[level.n_walls++] = (Wall) {
    .x1 = 64,
    .y1 = 0,
    .x2 = -1,
    .y2 = 0,
    .orientation = SOUTH,
  };
  level.walls[level.n_walls++] = (Wall) {
    .x1 = -1,
    .y1 = 63,
    .x2 = 64,
    .y2 = 63,
    .orientation = NORTH,
  };
  level.walls[level.n_walls++] = (Wall) {
    .x1 = 63,
    .y1 = 64,
    .x2 = 63,
    .y2 = -1,
    .orientation = WEST,
  };
  level.walls[level.n_walls++] = (Wall) {
    .x1 = 0,
    .y1 = -1,
    .x2 = 0,
    .y2 = 64,
    .orientation = EAST,
  };
}

static void
initialize_level_stars(uint8_t number)
{
  Coord size =
    (level.level_data->room_size == STANDARD_ROOM) ?
      STAR_SIZE
      :
      (STAR_SIZE / 2);
  Coord size_correction =
    (level.level_data->room_size == STANDARD_ROOM) ?
      1
      :
      0;
  uint8_t star_number;
  for (
      star_number = 0;
      star_number < level_data[number].n_stars;
      star_number++) {
    Star new_star = level_data[number].stars[star_number];
    level.walls[level.n_walls++] = (Wall) {
      .x1 = new_star.x,
        .y1 = new_star.y - size + 1,
        .x2 = new_star.x + size - 1,
        .y2 = new_star.y,
        .orientation = NORTHEAST,
    };
    level.walls[level.n_walls++] = (Wall) {
      .x1 = new_star.x + size - 1,
        .y1 = new_star.y + size_correction,
        .x2 = new_star.x,
        .y2 = new_star.y + size + size_correction - 1,
        .orientation = SOUTHEAST,
    };
    level.walls[level.n_walls++] = (Wall) {
      .x1 = new_star.x - size_correction,
        .y1 = new_star.y + size + size_correction - 1,
        .x2 = new_star.x - size - size_correction + 1,
        .y2 = new_star.y + size_correction,
        .orientation = SOUTHWEST,
    };
    level.walls[level.n_walls++] = (Wall) {
      .x1 = new_star.x - size - size_correction + 1,
        .y1 = new_star.y,
        .x2 = new_star.x - size_correction,
        .y2 = new_star.y - size + 1,
        .orientation = NORTHWEST,
    };
  }
}

static void
initialize_level_walls(uint8_t number)
{
  level.n_walls = 0;
  initialize_level_sides();
  initialize_level_stars(number);
}

static uint8_t
trooper_collision(Trooper *t1, Trooper *t2);
static void
advance_trooper(Trooper *trooper);

static void
queue_trooper_for_entry(Trooper *trooper, uint8_t pos, Doorway *doorway)
{
  place_trooper_at_doorway(trooper, doorway);
  trooper->direction = trooper->bearing = sum_angles(trooper->bearing, PI);
  trooper->speed = HI_RES(20 * pos);
  advance_trooper(trooper);
  trooper->direction = trooper->bearing = sum_angles(trooper->bearing, PI);
  trooper->speed = HI_RES(1);
}

static void
initialize_level_troopers(void)
{
  uint8_t i;
  for (i = 0; i <= level.level_data->n_opponents; i++) {
    level.troopers[i] = (Trooper) {
      .x = 0,
        .y = 0,
        .speed = 0,
        .direction = 0,
        .bearing = 0,
        .look = DEFAULT_LOOK_ANGLE,
        .base_rotation_speed = 0,
        .turning_speed = 1,
        .head_turning_speed = 1,
        .status = FLYING,
        .thrust = 0,
        .thrust_delta = THRUST_DELTA,
        .is_shooting = 0,
        .is_frozen = 0,
        .is_hit = 0,
        .frames_hit_until_frozen = FRAMES_TO_FROZEN,
        .action = ENTER_ROOM,
    };
    queue_trooper_for_entry(
        &level.troopers[i], i, &level.level_data->goal_doorway);
  }
  player.trooper = &level.troopers[0];
  queue_trooper_for_entry(player.trooper, 1, &level.level_data->start_doorway);
}

static void
initialize_level(uint8_t number)
{
  level = (Level){
    .level_data = &level_data[number],
#ifdef SMALLER_RAM
    .level_time = LEVEL_TIME * SAF_FPS,
    .remaining_frames = LEVEL_TIME * SAF_FPS,
#else
    .level_time = level_data[number].time * SAF_FPS,
    .remaining_frames = level_data[number].time * SAF_FPS,
#endif
    .surrender_frames = 0,
    .status = PLAYING,
    .troopers = { { 0 } },
    .walls = { { 0 } },
    .n_walls = 0,
  };
  initialize_level_troopers();

  /* Walls */
  initialize_level_walls(number);
}

typedef struct {
  char name[5];
  uint32_t score;
} LeaderboardEntry;

static char player_name[5];

#define LEADERBOARD_START 4
#define LEADERBOARD_ENTRIES 3
static LeaderboardEntry leaderboard[LEADERBOARD_ENTRIES] = { 0 };
static uint8_t scored_hi_score;

static void
check_leaderboard_status(uint32_t score)
{
  if (score > leaderboard[LEADERBOARD_ENTRIES - 1].score) {
    scored_hi_score = 1;
  }
}

static void
update_leaderboard_status(uint32_t score)
{
  int8_t j;
  for (j = LEADERBOARD_ENTRIES - 1; j >= 0; j--) {
    if (score > leaderboard[j].score) {
      if (j < LEADERBOARD_ENTRIES - 1) {
        leaderboard[j+1] = leaderboard[j];
      }
      uint8_t i;
      for (i = 0;
          (*(leaderboard[j].name + i) = *(player_name + i)) != 0; i++) {
        leaderboard[j].score = score;
      }
    }
  }
}

static void
load_leaderboard(void)
{
  uint8_t i = 0;
  for (i = 0; i < LEADERBOARD_ENTRIES; i++) {
    uint32_t encoded_name =
      (SAF_load(6 * i + LEADERBOARD_START + 0) << 16) +
      (SAF_load(6 * i + LEADERBOARD_START + 1) << 8) +
      (SAF_load(6 * i + LEADERBOARD_START + 2));

    leaderboard[i].name[0] = 32 + (encoded_name >> 18);
    leaderboard[i].name[1] = 32 + ((encoded_name >> 12) & 63);
    leaderboard[i].name[2] = 32 + ((encoded_name >> 6) & 63);
    leaderboard[i].name[3] = 32 + ((encoded_name) & 63);
    leaderboard[i].name[4] = 0; /* Unnecessary? */

    leaderboard[i].score =
      (SAF_load(6 * i + LEADERBOARD_START + 3) << 16) +
      (SAF_load(6 * i + LEADERBOARD_START + 4) << 8) +
      (SAF_load(6 * i + LEADERBOARD_START + 5));
  }
}

static void
save_score(uint32_t score, uint8_t start)
{
    SAF_save(start,  score >> 16);
    SAF_save(start + 1, (score >> 8) & 0xff);
    SAF_save(start + 2,  score & 0xff);
}

static uint32_t
load_score(uint8_t start)
{
  return
    (SAF_load(start) << 16) +
    (SAF_load(start + 1) <<8) +
    SAF_load(start + 2);
}

static void
save_leaderboard(void)
{
  uint8_t i;
  for (i = 0; i < LEADERBOARD_ENTRIES; i++) {
    uint32_t encoded_name =
      ((leaderboard[i].name[0] - 32) << 18) +
      ((leaderboard[i].name[1] - 32) << 12) +
      ((leaderboard[i].name[2] - 32) << 6) +
      ((leaderboard[i].name[3] - 32));
    SAF_save(6 * i + LEADERBOARD_START + 0, encoded_name >> 16);
    SAF_save(6 * i + LEADERBOARD_START + 1, (encoded_name >> 8) & 0xff);
    SAF_save(6 * i + LEADERBOARD_START + 2, encoded_name & 0xff);

    save_score(leaderboard[i].score, 6 * i + LEADERBOARD_START + 3);
  }
}

#include "drawing_code.h"

/* Game loops */

static uint8_t (*active_loop)(void);
static uint8_t (*next_loop)(void);
static uint8_t room_title_loop(void);
static uint8_t title_loop(void);
static uint8_t game_loop(void);
static uint8_t level_number;
static void init_game(void);
static uint32_t score;

static uint8_t
black_transition_loop(void)
{
  const uint8_t transition_frames = SAF_FPS / 5;
  static uint8_t remaining_frames = transition_frames;
  SAF_clearScreen(SAF_COLOR_BLACK);
  if (!--remaining_frames) {
    remaining_frames = transition_frames; /* Re-init */
    active_loop = next_loop;
  }
  return 1;
}

static uint8_t
alphabet_loop(void)
{
  static char result[5] = "   ";
  static uint8_t pos = 0;
  static int8_t selected = 32;
  static uint8_t step = 8;
  static int dest_x = 8 * 32; /* selected * step */
  static int curr_x = 8 * 32; /* dest_x */
  static const uint8_t stripe_y = 48;


  const uint8_t name_length = 4;
  char c[2] = { 0 };

  SAF_clearScreen(SAF_COLOR_WHITE);

  /* result */

  SAF_drawRect(
      32 - 2 * 12 - 1,
      stripe_y - 28, 12 * name_length,
      12,
      SAF_COLOR_WHITE,
      1);
  SAF_drawRect(
      32 - 2 * 12 - 1,
      stripe_y - 28,
      12 * name_length,
      12,
      SAF_COLOR_BLACK,
      0);
  SAF_drawText(
      result, 32 - 2 * 12 + 1, stripe_y - 28 + 2, SAF_COLOR_BLACK, 2);
  if (((SAF_frame()>>3)%2) && (pos < name_length)) {
    SAF_drawLine(
        32 - 2 * 12 + 1 + 12 * pos,
        stripe_y - 28 + 9,
        32 - 2 * 12 + 8 + 12 * pos,
        stripe_y - 28 + 9,
        SAF_COLOR_BLACK);
  }

  /* alphabet */

  curr_x += (curr_x < dest_x) ? 2 : (curr_x > dest_x) ? -2 : 0;
  uint8_t ch;
  for (ch = 0; ch < 64 ; ch++) {
      *c = 32 + ch;
      if ((step * ch - curr_x >= - 40) && (step * ch - curr_x <= 96)) {
        SAF_drawRect(
            32 - 4 + step * ch - curr_x,
            stripe_y - 4,
            8,
            8,
            SAF_COLOR_BLACK,
            0);
        SAF_drawText(
            c, 32 - 2 + step * ch - curr_x, stripe_y - 2, SAF_COLOR_BLACK, 1);
      }
  }
  selected +=
    SAF_buttonJustPressed(SAF_BUTTON_RIGHT) -
    SAF_buttonJustPressed(SAF_BUTTON_LEFT);
  if ((selected < 0) || (selected > 63)) {
    selected &= 63;
    curr_x = step * selected;
  }
  dest_x = step * selected;
  *c = 32 + selected;
  SAF_drawRect(32 - 8, stripe_y - 8, 16, 16, SAF_COLOR_WHITE, 1);
  SAF_drawRect(32 - 8, stripe_y - 8, 16, 16, SAF_COLOR_BLACK, 0);
  SAF_drawText(c, 32 - 8 + 2, stripe_y - 8 + 2, SAF_COLOR_BLACK, 3);

  if (SAF_buttonJustPressed(SAF_BUTTON_A) ||
      SAF_buttonJustPressed(SAF_BUTTON_UP)) {
    if (pos == name_length) {
      *((uint32_t *)player_name) = *((uint32_t *)result);
      update_leaderboard_status(score);
      save_leaderboard();
      init_game();
      return 1;
    }
    *(result + pos++) = *c;
  }

  if ((SAF_buttonJustPressed(SAF_BUTTON_B) ||
        SAF_buttonJustPressed(SAF_BUTTON_DOWN)) && (pos > 0)) {
    *(result + --pos) = ' ';
  }

  return 1;
}

static uint8_t
endgame_loop(void)
{

  char score_text[12];

  SAF_clearScreen(SAF_COLOR_WHITE);
  centerTextWithBorder(1, "GAME", SAF_COLOR_WHITE, SAF_COLOR_BLACK, 2);
  centerTextWithBorder(4, "OVER", SAF_COLOR_WHITE, SAF_COLOR_BLACK, 2);

  centerTextWithBorder(7, "score", SAF_COLOR_WHITE, SAF_COLOR_BLACK, 1);
  centerTextWithBorder(
      9,
      SAF_intToStr(score, score_text),
      SAF_COLOR_WHITE,
      SAF_COLOR_BLACK,
      1);

  if (anyButtonPressed()) {
    if (scored_hi_score) {
      active_loop = alphabet_loop;
    } else {
      init_game();
    }
  }
  return 1;
}

static void
check_level_status(void);
static void
apply_user_input(void);
static void
apply_troopers_actions(void);
static void
bounce_troopers(void);
static void
move_troopers(void);
static void
shoot_lightguns(void);

static uint8_t
game_loop(void)
{
  const uint8_t message_frames = 5 * SAF_FPS;
  static uint8_t remaining_frames = message_frames;

  if (!--level.remaining_frames &&
      (level.status != TIMED_OUT) &&
      (level.status != CLEARED)) {
    remaining_frames = message_frames;
    level.status = TIMED_OUT;
  }

  if (
      ((level.status == TIMED_OUT) ||
       (level.status == SURRENDER) ||
       (level.status == LOST)) &&
      (!--remaining_frames || anyButtonPressed())) {
    active_loop = endgame_loop;
  } else if ((level.status == CLEARED) &&
      (!--remaining_frames || anyButtonPressed())) {
    remaining_frames = message_frames; /* Re-init */
    level_number += level_number < (n_levels - 1) ? 1 : 0;
    next_loop = room_title_loop;
    active_loop = black_transition_loop;
  }

  draw_game();
  apply_troopers_actions();
  move_troopers();
  check_level_status(); /* Order is important,  */
  bounce_troopers();    /* 1. move,             */
  shoot_lightguns();    /* 2. check,            */
                        /* 3. bounce,           */
                        /* 4. shoot             */
  return 1;
}

static void
save_level_continue(uint8_t level_number)
{
  SAF_save(0, level_number);
  save_score(score, 1);
}

static void
load_level_continue(uint8_t *level_number, uint32_t *score)
{
  *level_number = SAF_load(0);
  *score = load_score(1);
}

static uint8_t
room_title_loop(void)
{
  const uint8_t title_frames = 99;
  static uint8_t remaining_frames = title_frames;
  char timeout[2];
  Color colors[4] = {
#ifdef MONOCHROME
    SAF_COLOR_BLACK,
    SAF_COLOR_BLACK,
    SAF_COLOR_BLACK,
    SAF_COLOR_BLACK
#else
    SAF_COLOR_RED,
    SAF_COLOR_RED_DARK,
    SAF_COLOR_YELLOW,
    SAF_COLOR_GREEN_DARK
#endif
  };
  Color text_color = SAF_COLOR_BLACK;
  Color shadow_color = SAF_COLOR_WHITE;

  if (remaining_frames == title_frames) {
    initialize_level(level_number);
    save_level_continue(level_number);
  };

  SAF_clearScreen(SAF_COLOR_BLACK);
  SAF_drawPixel(SAF_random() >> 2, SAF_random() >> 2, SAF_COLOR_WHITE);
  SAF_drawPixel(SAF_random() >> 2, SAF_random() >> 2, SAF_COLOR_WHITE);

  centerTextWithBorder(1, "Entering", text_color, shadow_color, 1);

  char level_text[18] = "level 88888888888";
  SAF_intToStr(level_number, level_text + 6);
  centerTextWithBorder(2, level_text, text_color, shadow_color, 1);

#ifdef SMALLER_RAM
#else
  char time_text[] = "Time 0:00";
  time_text[5] = '0' + level_data[level_number].time / 60;
  time_text[7] = '0' + level_data[level_number].time % 60 / 10;
  time_text[8] = '0' + level_data[level_number].time % 60 % 10;
  centerTextWithBorder(4, time_text, text_color, shadow_color, 1);
#endif
  centerTextWithBorder(6, "score", text_color, shadow_color, 1);
  char score_text[12];
  centerTextWithBorder(
      7, SAF_intToStr(score, score_text), text_color, shadow_color, 1);

  timeout[0] = '0' + remaining_frames / SAF_FPS;
  timeout[1] = 0;
  centerTextWithBorder(
      9, timeout, colors[timeout[0] - '0'], shadow_color, 2);

  if (!--remaining_frames) {
    remaining_frames = title_frames; /* Re-init */
    next_loop = game_loop;
    active_loop = black_transition_loop;
  }
  return 1;
}

static uint8_t
dissolve_loop(void)
{
  static uint8_t remaining_frames = 50;
  uint8_t x, y;
  uint8_t xc1 = SAF_random() >> 4;
  uint8_t yc1 = SAF_random() >> 4;
  uint8_t xc2 = SAF_random() >> 4;
  uint8_t yc2 = SAF_random() >> 4;
  uint8_t xc3 = SAF_random() >> 4;
  uint8_t yc3 = SAF_random() >> 4;
  for (x = 0; x < 63; x+=8) {
    for (y = 0; y < 63; y+=8) {
      SAF_drawPixel(x + xc1, y + yc1, SAF_COLOR_BLACK);
      SAF_drawPixel(x + xc2, y + yc2, SAF_COLOR_BLACK);
      SAF_drawPixel(x + xc3, y + yc3, SAF_COLOR_BLACK);
      SAF_drawPixel(63 - x - xc1, 63 - y - yc1, SAF_COLOR_BLACK);
      SAF_drawPixel(63 - x - xc2, 63 - y - yc2, SAF_COLOR_BLACK);
      SAF_drawPixel(63 - x - xc3, 63 - y - yc3, SAF_COLOR_BLACK);
    }
  }
  if (!--remaining_frames || anyButtonPressed()) {
    remaining_frames = 50; /* Re-init */
    active_loop = room_title_loop;
  }
  return 1;
}

static void
start_or_continue_game(void)
{
  if (SAF_buttonPressed(SAF_BUTTON_B)) {
    load_level_continue(&level_number, &score);
  }
  active_loop = dissolve_loop;
}

static uint8_t
leaderboard_loop(void)
{
  static uint8_t initial_remaining_frames = 5 * SAF_FPS;
  static uint8_t remaining_frames = 5 * SAF_FPS;
  if (remaining_frames == initial_remaining_frames) {
    SAF_clearScreen(SAF_COLOR_WHITE);
    centerTextWithBorder(
        1, "TOP", SAF_COLOR_WHITE, SAF_COLOR_BLACK, 1);
    centerTextWithBorder(
        2, "SCORES", SAF_COLOR_WHITE, SAF_COLOR_BLACK, 1);
    uint8_t position;
    for (position = 0; position < LEADERBOARD_ENTRIES; position++) {
      if (leaderboard[position].score > 0) {
        char score_txt[9] = "88888888";
        centerTextWithBorder(
            4+position * 2,
            leaderboard[position].name,
            SAF_COLOR_BLACK,
            SAF_COLOR_WHITE,
            1);
        centerTextWithBorder(
            5+position * 2,
            SAF_intToStr(leaderboard[position].score, score_txt),
            SAF_COLOR_WHITE,
            SAF_COLOR_BLACK,
            1);
      }
    }
  }

  if (!--remaining_frames) {
    remaining_frames = initial_remaining_frames; /* Re-init */
    active_loop = title_loop;
  }
  if (anyButtonPressed()) {
    remaining_frames = initial_remaining_frames; /* Re-init */
    start_or_continue_game();
  }
  return 1;
}

static uint8_t
instructions_loop(void)
{
  static uint8_t initial_remaining_frames = 10 * SAF_FPS;
  static uint8_t remaining_frames = 10 * SAF_FPS;
  char *keys[9] = {
    " Controls  ",
    "",
    " B: cling",
    "    propel",
    "    shake",
    "    give up",
    "LR: turn",
    "UD: aim",
    " A: shoot",
  };
  if (remaining_frames == initial_remaining_frames) {
    SAF_clearScreen(SAF_COLOR_WHITE);
    uint8_t i;
    for (i = 0; i < 9; i++) {
      drawTextWithBorder(
          keys[i],
          1,
          i*7+2,
          SAF_COLOR_WHITE,
          SAF_COLOR_BLACK,
          1);
    }
  }
  if (!--remaining_frames) {
    remaining_frames = initial_remaining_frames; /* Re-init */
    if (leaderboard[0].score > 0) {
      active_loop = leaderboard_loop;
    } else {
      active_loop = title_loop;
    }
  }
  if (anyButtonPressed()) {
    remaining_frames = initial_remaining_frames; /* Re-init */
    start_or_continue_game();
  }
  return 1;
}

static uint8_t
title_loop(void)
{
  static uint8_t initial_remaining_frames = 5 * SAF_FPS;
  static uint8_t remaining_frames = 5 * SAF_FPS;
  if (remaining_frames == initial_remaining_frames) {
    SAF_clearScreen(LT_BACKGROUND_COLOR);
    centerTextWithBorder(3, "BATL", SAF_COLOR_WHITE, SAF_COLOR_BLACK, 2);
    centerTextWithBorder(6, "ROOM", SAF_COLOR_WHITE, SAF_COLOR_BLACK, 2);
    centerTextWithBorder(9, "A: begin", SAF_COLOR_BLACK, SAF_COLOR_WHITE, 1);
    centerTextWithBorder(
        10, "B: continue", SAF_COLOR_BLACK, SAF_COLOR_WHITE, 1);
  }
  if (!--remaining_frames) {
    remaining_frames = initial_remaining_frames; /* Re-init */
    active_loop = instructions_loop;
  }
  if (anyButtonPressed()) {
    remaining_frames = initial_remaining_frames; /* Re-init */
    start_or_continue_game();
  }
  return 1;
}

typedef struct {
  HiResCoord x;
  HiResCoord y;
  HiResValue /*Magnitude*/ distance;
  Trooper *trooper;
} Touchpoint;

/* https://stackoverflow.com/questions/15311018/c-procedure-for-determining-whether-two-segments-intersect */
/* From https://stackoverflow.com/a/1968345 */
/* Returns 1 if the lines intersect, otherwise 0. In addition, if the lines  */
/* intersect the intersection point may be stored in the floats i_x and i_y. */

static char
get_line_intersection(float p0_x, float p0_y, float p1_x, float p1_y,
    float p2_x, float p2_y, float p3_x, float p3_y, float *i_x, float *i_y)
{
    float s1_x, s1_y, s2_x, s2_y;
    s1_x = p1_x - p0_x;     s1_y = p1_y - p0_y;
    s2_x = p3_x - p2_x;     s2_y = p3_y - p2_y;

    float s, t;
    s = (-s1_y * (p0_x - p2_x) + s1_x * (p0_y - p2_y)) /
      (-s2_x * s1_y + s1_x * s2_y);
    t = ( s2_x * (p0_y - p2_y) - s2_y * (p0_x - p2_x)) /
      (-s2_x * s1_y + s1_x * s2_y);

    if (s >= 0 && s <= 1 && t >= 0 && t <= 1)
    {
        /* Collision detected */
        if (i_x != 0)
            *i_x = (p0_x + (t * s1_x));
        if (i_y != 0)
            *i_y = (p0_y + (t * s1_y));
        return 1;
    }

    return 0; /* No collision */
}

#define H(x) ((x) << 8)
#define L(x) ((x) >> 8)

static Touchpoint
calculate_touchpoint(
    HiResCoord ax,
    HiResCoord ay,
    HiResCoord bx,
    HiResCoord by,
    HiResCoord cx,
    HiResCoord cy,
    HiResCoord dx,
    HiResCoord dy)
{
  Touchpoint result = { .x = 0, .y = 0, .distance = -1, .trooper = 0 };
  float ox, oy;
  if (get_line_intersection(ax, ay, bx, by, cx, cy, dx, dy, &ox, &oy)) {
    result.x = ox;
    result.y = oy;
    result.distance =
      ((result.x - ax) * (result.x - ax)) +
      ((result.y - ay) * (result.y - ay));
  }
  return result;
}

static void
freeze(Trooper *trooper)
{
  trooper->is_frozen = 1;
}

static void
hit(Trooper *trooper)
{
  trooper->is_hit = 1;
  if (!--trooper->frames_hit_until_frozen) {
    freeze(trooper);
  }
}

static void
shoot_lightgun(Trooper *trooper)
{
  Touchpoint closest_crossing = { 0 };
  closest_crossing.distance = -1;
  /* Calculate shot segment */

  HiResCoord far_distance = H(100); /* surely > sqrt(2*64^2) */
  Angle visor_angle = sum_angles(trooper->bearing, trooper->look);
  HiResCoord lightgun_x =
    H(LO_RES(trooper->x -
    (level.level_data->room_size == LARGE_ROOM ?
     0
     :
     sine_projection(FARVIEW_HELMET_RADIUS, trooper->bearing) * 2) +
      cosine_projection(
        CLOSEUP_FOREARM_LENGTH, sum_angles(trooper->bearing, trooper->look))));

  HiResCoord lightgun_y =
    H(LO_RES(trooper->y +
    (level.level_data->room_size == LARGE_ROOM ?
     0
     :
     cosine_projection(FARVIEW_HELMET_RADIUS, trooper->bearing) * 2) +
      sine_projection(
        CLOSEUP_FOREARM_LENGTH, sum_angles(trooper->bearing, trooper->look))));
  if (
      (lightgun_x < 0) ||
      (lightgun_x > H(63)) ||
      (lightgun_y < 0) ||
      (lightgun_y > H(63))) {
    trooper->is_shooting = 0;
    return;
  }
  HiResCoord far_point_x =
    (lightgun_x + cosine_projection(far_distance, visor_angle));
  HiResCoord far_point_y =
    (lightgun_y +   sine_projection(far_distance, visor_angle));

  /* Find closest crossing wall */
  uint8_t i;
  for (i = 0; i < level.n_walls; i++) {
    Touchpoint p = calculate_touchpoint(
          lightgun_x,
          lightgun_y,
          far_point_x,
          far_point_y,
          H(level.walls[i].x1),
          H(level.walls[i].y1),
          H(level.walls[i].x2),
          H(level.walls[i].y2));
    if (
        p.distance >= 0 &&
        (
         (closest_crossing.distance == -1) ||
         (p.distance < closest_crossing.distance))) {
      closest_crossing = p;
    }
  }

  /* Find closest crossing trooper */
  HiResCoord diameter =
    level.level_data->room_size == LARGE_ROOM ?
    HI_RES(1)
    :
    FARVIEW_HELMET_RADIUS;
  for (i = 0; i <= level.level_data->n_opponents; i++) {
    if (&level.troopers[i] == trooper)
      continue;
    Touchpoint p = calculate_touchpoint(
          lightgun_x,
          lightgun_y,
          far_point_x,
          far_point_y,
          H(LO_RES(level.troopers[i].x -
              sine_projection(diameter, trooper->bearing))),
          H(LO_RES(level.troopers[i].y +
              cosine_projection(diameter, trooper->bearing))),
          H(LO_RES(level.troopers[i].x +
              sine_projection(diameter, trooper->bearing))),
          H(LO_RES(level.troopers[i].y -
              cosine_projection(diameter, trooper->bearing))));
    if (
        p.distance >= 0 &&
        (
         (closest_crossing.distance == -1) ||
         (p.distance < closest_crossing.distance))) {
      closest_crossing = p;
      closest_crossing.trooper = &level.troopers[i];
    }
    /* other diagonal */
    p = calculate_touchpoint(
          lightgun_x,
          lightgun_y,
          far_point_x,
          far_point_y,
          H(LO_RES(level.troopers[i].x -
              cosine_projection(diameter, trooper->bearing))),
          H(LO_RES(level.troopers[i].y +
              sine_projection(diameter, trooper->bearing))),
          H(LO_RES(level.troopers[i].x +
              cosine_projection(diameter, trooper->bearing))),
          H(LO_RES(level.troopers[i].y -
              sine_projection(diameter, trooper->bearing))));
    if (
        p.distance >= 0 &&
        (
         (closest_crossing.distance == -1) ||
         (p.distance < closest_crossing.distance))) {
      closest_crossing = p;
      closest_crossing.trooper = &level.troopers[i];
    }
  }

  /* If trooper, hit it */
  if (closest_crossing.trooper) {
    hit(closest_crossing.trooper);
  }

  /* there should be one, even if only the external walls */
  trooper->point_of_impact_x = L(closest_crossing.x);
  trooper->point_of_impact_y = L(closest_crossing.y);
}

static void
shoot_lightguns(void)
{
  uint8_t i;
  for (i = 0; i <= level.level_data->n_opponents; i++) {
    level.troopers[i].is_hit = 0;
  }
  for (i = 0; i <= level.level_data->n_opponents; i++) {
    if (level.troopers[i].is_shooting) {
      shoot_lightgun(&level.troopers[i]);
    }
  }
  for (i = 0; i <= level.level_data->n_opponents; i++) {
    if (!level.troopers[i].is_hit) {
      level.troopers[i].frames_hit_until_frozen = FRAMES_TO_FROZEN;
    }
  }
}

static void
let_trooper_cling(Trooper *trooper)
{
  trooper->speed = 0;
  trooper->base_rotation_speed = 0;
  trooper->status = CLINGING;
}

static Angle
effective_rotation_speed(Trooper *trooper)
{
  Angle arm_extension_range =
    subtract_angles(MAX_LOOK_ANGLE, MIN_LOOK_ANGLE);
  Angle arm_extension_correction =
    subtract_angles(trooper->look, MIN_LOOK_ANGLE);

  return adjust_angle_by(
    (Fraction){
      .numerator =
        (arm_extension_range / 5 * 6 - arm_extension_correction) / 10000,
      .denominator = arm_extension_range / 10000
      },
    trooper->base_rotation_speed);
}

static void
bump_trooper_against_wall(
    Trooper *trooper,
    Angle old_direction,
    Angle new_direction,
    Angle normal_angle)
{

  if ((trooper->status == ATTEMPTING_TO_CLING) && !trooper->is_frozen) {
      let_trooper_cling(trooper);
  } else if (trooper->is_frozen || (trooper->status == FLYING)) {

    trooper->direction = new_direction;

    /* Affect direction and speed because of wall friction (function of the
     * effective rotation speed)*/

    Angle direction_adjustment =
      adjust_angle_by(BUMP_COEF, effective_rotation_speed(trooper));
    trooper->direction =
      sum_angles(trooper->direction, direction_adjustment);
    trooper->speed = adjust_by(WALL_LINEAR_FRICTION_COEF, trooper->speed);

    /* Affect the rotation by wall friction: if we don't do this, we will
     * never rotate */

    Angle diff = adjust_angle_by(
        (Fraction){
          .numerator = 1,
          .denominator = 10
        },
        subtract_angles(normal_angle, old_direction));
    trooper->base_rotation_speed =
      subtract_angles(trooper->base_rotation_speed,
          diff);
    trooper->base_rotation_speed =
       adjust_angle_by(
           WALL_ANGULAR_FRICTION_COEF, trooper->base_rotation_speed);
    SAF_playSound(SAF_SOUND_BUMP);
  }
}

static void
rotate_trooper(Trooper *trooper)
{
  /* Air friction */
  trooper->base_rotation_speed =
    adjust_angle_by(AIR_ANGULAR_FRICTION_COEF, trooper->base_rotation_speed);

  /* Shape factor, rotational inertia */
  trooper->bearing =
    sum_angles(trooper->bearing,
        effective_rotation_speed(trooper));
}

static uint8_t
wall_collision(Trooper *trooper, Wall *wall);

static uint8_t
arriving_at_doorway(Trooper *trooper, Doorway *doorway)
{
  return wall_collision(trooper, doorway);
}

static void
advance_trooper(Trooper *trooper)
{
  trooper->x += cosine_projection(trooper->speed, trooper->direction);
  trooper->y += sine_projection(trooper->speed, trooper->direction);
}

static void
retreat_trooper(Trooper *trooper)
{
  trooper->x -= cosine_projection(trooper->speed, trooper->direction);
  trooper->y -= sine_projection(trooper->speed, trooper->direction);
}

static void
translate_trooper(Trooper *trooper)
{
  if (trooper->action == ENTER_ROOM) return;
  advance_trooper(trooper);

  /* Ensure inside bounds */
  if (trooper->x > HI_RES(63)) trooper->x = HI_RES(63);
  if (trooper->x <= HI_RES(0)) trooper->x = HI_RES(0);
  if (trooper->y > HI_RES(63)) trooper->y = HI_RES(63);
  if (trooper->y <= HI_RES(0)) trooper->y = HI_RES(0);

  /* Air friction */
  trooper->speed = adjust_by(AIR_LINEAR_FRICTION_COEF, trooper->speed);
}

static uint8_t
trooper_collision(Trooper *t1, Trooper *t2)
{
  Coord x = abs(LO_RES(t2->x - t1->x));
  Coord y = abs(LO_RES(t2->y - t1->y));

  return (x + y) < ((level.level_data->room_size == LARGE_ROOM) ? 5 : 8);
}

static Angle
my_limited_atan2(Coord x, Coord y)
{
  static int8_t atan_table[4][4] = {
    {32, 45, 51, 54},
    {19, 32, 40, 45},
    {13, 24, 32, 38},
    {10, 19, 26, 32},
  };

  Coord corrected_x = 0, corrected_y = 0;

  if ((abs(x) > 4) || (abs(y) > 4)) {
    if (x == 0) {
      corrected_y = (abs(y) <= 4) ? y : (4 * sgn(y));
      corrected_x = 0;
    } else if (y == 0) {
      corrected_x = (abs(x) <= 4) ? x : (4 * sgn(x));
      corrected_y = 0;
    } else if (abs(x) < abs(y)) {
      corrected_x = sgn(x) * 4 * abs(x) / abs(y);
      corrected_y = 4 * sgn(y);
    } else {
      corrected_y = sgn(y) * 4 * abs(y) / abs(x);
      corrected_x = 4 * sgn(x);
    }
  }
  /* Better safe than sorry */
  /* if (x < -4) x = -4;
  if (x >  4) x =  4;
  if (y < -4) y = -4;
  if (y >  4) y =  4;*/

  x = corrected_x;
  y = corrected_y;

  if (x > 0) {
    if (y > 0) {
      return HI_RES_ANGLE(atan_table[x - 1][y - 1]);
    } else if (y < 0) {
      return negated_angle(HI_RES_ANGLE(atan_table[x - 1][-y - 1]));
    } else { /* y == 0) */
      return 0;
    }
  } else if (x < 0) {
    if (y > 0) {
      return sum_angles(
          negated_angle(HI_RES_ANGLE(atan_table[-x - 1][y - 1])), PI);
    } else if (y < 0) {
      return sum_angles(HI_RES_ANGLE(atan_table[-x - 1][-y - 1]), PI);
    } else { /* y == 0 */
      return PI;
    }
  } else { /* x == 0 */
    if (y > 0) {
      return PI / 2;
    } else if (y < 0) {
      return PI / 2 * 3;
    } else { /* x ==0, y == 0 --> undefined, return for example 0 */
      return 0;
    }
  }
}

static Angle
troopers_collision_angle(Trooper *t1, Trooper *t2)
{
  Coord x = LO_RES(t2->x - t1->x);
  Coord y = LO_RES(t2->y - t1->y);
  return my_limited_atan2(x, y);
}

static void
shake_troopers(Trooper *t1, Trooper *t2)
{
  Trooper *t;
  if (t1 == player.trooper) {
    t = t1;
  } else if (t2 == player.trooper) {
    t = t2;
  } else {
    return;
  }
  if (t->status == ATTEMPTING_TO_CLING) {
    t->x += HI_RES((SAF_random() >> 5) - 4);
    t->y += HI_RES((SAF_random() >> 5) - 4);
  }
}

static void
calculate_troopers_collision(Trooper *t1, Trooper *t2)
{
  if (!trooper_collision(t1, t2)) {
    return;
  }
  /* Now we know they are colliding */

  if ((t1->status != FLYING) && (t1->status != ATTEMPTING_TO_CLING)) {
    t1->status = FLYING;
  }
  if ((t2->status != FLYING) && (t2->status != ATTEMPTING_TO_CLING)) {
    t2->status = FLYING;
  }

  retreat_trooper(t1);
  retreat_trooper(t2);

  Angle alpha = troopers_collision_angle(t1, t2);

  t1->direction = sum_angles(alpha, sum_angles(PI / 2, t1->direction));
  t2->direction = sum_angles(alpha, sum_angles(PI / 2 * 3, t2->direction));

  t1->speed = t2->speed = (t1->speed + t2->speed) / 2;

  translate_trooper(t1); /* Double translation to try to avoid hanging */
  translate_trooper(t2); /* It seems to work although I don't know how/why */

  Angle aux = t1->base_rotation_speed;
  t1->base_rotation_speed =
    sum_angles(
        adjust_angle_by(HALF, t1->base_rotation_speed),
        adjust_angle_by(HALF, t2->base_rotation_speed));
  t2->base_rotation_speed =
    sum_angles(
        adjust_angle_by(HALF, t2->base_rotation_speed),
        adjust_angle_by(HALF, aux));

  SAF_playSound(SAF_SOUND_BUMP);

  shake_troopers(t1, t2);
}

static uint8_t
opposite_sides(Angle a, Angle b)
{
  Angle diff =
    (a > b) ?
    sum_angles(b, subtract_angles(TWOPI, a)) :
    sum_angles(a, subtract_angles(TWOPI, b));
  return (diff > PI / 2) && (diff < PI / 2 * 3);
}

static uint8_t
wall_collision(Trooper *trooper, Wall *wall)
{
  /* All walls move clockwise */
  HiResCoord local_x, local_y;
  HiResCoord x1 = HI_RES(wall->x1);
  HiResCoord x2 = HI_RES(wall->x2);
  HiResCoord y1 = HI_RES(wall->y1);
  HiResCoord y2 = HI_RES(wall->y2);
  switch (wall->orientation) {
  case NORTH:
    return
      (trooper->x > x1) &&
      (trooper->x < x2) &&
      (abs(y1 - trooper->y) <= HI_RES(2)) &&
      opposite_sides(trooper->direction, wall_normals[wall->orientation]);
  case SOUTH:
    return
      (trooper->x > x2) &&
      (trooper->x < x1) &&
      (abs(trooper->y - y2) <= HI_RES(2)) &&
      opposite_sides(trooper->direction, wall_normals[wall->orientation]);
  case EAST:
    return
      (trooper->y > y1) &&
      (trooper->y < y2) &&
      (abs(trooper->x - x1) <= HI_RES(2)) &&
      opposite_sides(trooper->direction, wall_normals[wall->orientation]);
  case WEST:
    return
      (trooper->y < y1) &&
      (trooper->y > y2) &&
      (abs(x1 - trooper->x) <= HI_RES(2)) &&
      opposite_sides(trooper->direction, wall_normals[wall->orientation]);
  case NORTHEAST:
    local_x = trooper->x - x1;
    local_y = trooper->y - y1;
    return
      (trooper->x > x1) &&
      (trooper->x < x2) &&
      (trooper->y > y1) &&
      (trooper->y < y2) &&
      (abs(local_x - local_y) <= HI_RES(2)) &&
      opposite_sides(trooper->direction, wall_normals[wall->orientation]);
  case SOUTHEAST:
    local_x = x1 - trooper->x;
    local_y = trooper->y - y1;
    return
      (trooper->x > x2) &&
      (trooper->x < x1) &&
      (trooper->y > y1) &&
      (trooper->y < y2) &&
      (abs(local_y - local_x) <= HI_RES(2)) &&
      opposite_sides(trooper->direction, wall_normals[wall->orientation]);
  case SOUTHWEST:
    local_x = trooper->x - x2;
    local_y = trooper->y - y2;
    return
      (trooper->x > x2) &&
      (trooper->x < x1) &&
      (trooper->y > y2) &&
      (trooper->y < y1) &&
      (abs(local_y - local_x) <= HI_RES(2)) &&
      opposite_sides(trooper->direction, wall_normals[wall->orientation]);
  case NORTHWEST:
    local_x = x2 - trooper->x;
    local_y = trooper->y - y2;
    return
      (trooper->x > x1) &&
      (trooper->x < x2) &&
      (trooper->y > y2) &&
      (trooper->y < y1) &&
      (abs(local_x - local_y) <= HI_RES(2)) &&
      opposite_sides(trooper->direction, wall_normals[wall->orientation]);
  default:
    return 0;
  }
}

static void
calculate_wall_collision(Trooper *trooper, Wall *wall)
{
  if (!wall_collision(trooper, wall)) {
    return;
  }

  /* Now we know we collided */

  retreat_trooper(trooper);

  Angle new_direction =
    sum_angles(
        PI,
        subtract_angles(
          sum_angles(
            wall_normals[wall->orientation],
            wall_normals[wall->orientation]),
          trooper->direction));

  bump_trooper_against_wall(
      trooper,
      trooper->direction,
      new_direction,
      wall_normals[wall->orientation]);
  translate_trooper(trooper);
}

static void
bounce_troopers(void)
{
  uint8_t i, j, w;
  for (i = 1; i <= level.level_data->n_opponents; i++) {
    for (j = 0; j < i; j++) {
      if (
          (level.troopers[i].action != ENTER_ROOM) &&
          (level.troopers[j].action != ENTER_ROOM)) {
        calculate_troopers_collision(&level.troopers[i], &level.troopers[j]);
      }
    }
  }
  for (i = 0; i <= level.level_data->n_opponents; i++) {
    if (level.troopers[i].action != ENTER_ROOM) {
      for (w = 0; w < level.n_walls; w++) {
        calculate_wall_collision(&level.troopers[i], &level.walls[w]);
      }
    }
  }
}

static void
increase_trooper_thrust(Trooper *trooper)
{
  trooper->thrust += trooper->thrust_delta;
  if (trooper->thrust < MIN_THRUST) {
    trooper->thrust = MIN_THRUST;
    trooper->thrust_delta = -trooper->thrust_delta;
  } else if (trooper->thrust > MAX_THRUST) {
    trooper->thrust = MAX_THRUST;
    trooper->thrust_delta = -trooper->thrust_delta;
  }
}

static void
propel_trooper(Trooper *trooper)
{
  SAF_playSound(SAF_SOUND_BOOM);
  trooper->direction = trooper->bearing;
  trooper->speed = HI_RES(trooper->thrust) / MAX_THRUST;
  if (level.level_data->room_size == LARGE_ROOM) {
    trooper->speed /= 2;
  }
  trooper->status = FLYING;
}

static void
turn_counterclockwise(Trooper *trooper)
{
  trooper->bearing =
    subtract_angles(trooper->bearing, HI_RES_ANGLE(trooper->turning_speed));
  trooper->direction = trooper->bearing;
  trooper->turning_speed *= 2;
  if (trooper->turning_speed > TURNING_SPEED) {
    trooper->turning_speed = TURNING_SPEED;
  }
}

static void
turn_clockwise(Trooper *trooper)
{
  trooper->bearing =
    sum_angles(trooper->bearing, HI_RES_ANGLE(trooper->turning_speed));
  trooper->direction = trooper->bearing;
  trooper->turning_speed *= 2;
  if (trooper->turning_speed > TURNING_SPEED) {
    trooper->turning_speed = TURNING_SPEED;
  }
}

static void
reset_turning_speed(Trooper *trooper)
{
  trooper->turning_speed = 1;
}

static void
turn_head_clockwise(Trooper *trooper)
{
  trooper->look =
    sum_angles(trooper->look, HI_RES_ANGLE(trooper->head_turning_speed));
  if ((trooper->look > MAX_LOOK_ANGLE) &&
      (trooper->look < MIN_LOOK_ANGLE)) {
    trooper->look = MAX_LOOK_ANGLE;
  }
  trooper->head_turning_speed *= 2;
  if (trooper->head_turning_speed > LOOK_TURNING_SPEED) {
    trooper->head_turning_speed = LOOK_TURNING_SPEED;
  }
}

static void
turn_head_counterclockwise(Trooper *trooper)
{
  trooper->look =
    subtract_angles(trooper->look, HI_RES_ANGLE(trooper->head_turning_speed));
  if ((trooper->look < MIN_LOOK_ANGLE) &&
      (trooper->look > MAX_LOOK_ANGLE)) {
    trooper->look = MIN_LOOK_ANGLE;
  }
  trooper->head_turning_speed *= 2;
  if (trooper->head_turning_speed > LOOK_TURNING_SPEED) {
    trooper->head_turning_speed = LOOK_TURNING_SPEED;
  }
}

static void
reset_head_turning_speed(Trooper *trooper)
{
  trooper->head_turning_speed = 1;
}

static void
apply_user_input(void)
{
  int button_pressed = 0;
  static uint8_t remaining_frames = SAF_FPS / 2;

  player.trooper->is_shooting = SAF_buttonPressed(SAF_BUTTON_A) &&
    !player.trooper->is_frozen;
  if (!player.trooper->is_frozen &&
      ((player.trooper->status == FLYING) ||
      (player.trooper->status == ATTEMPTING_TO_CLING) ||
      (player.trooper->status == GAINING_THRUST))) {
    if (SAF_buttonPressed(SAF_BUTTON_DOWN) &&
        !SAF_buttonPressed(SAF_BUTTON_UP)) {
      button_pressed = 1;
      turn_head_clockwise(player.trooper);
    } else if (SAF_buttonPressed(SAF_BUTTON_UP)) {
      button_pressed = 1;
      turn_head_counterclockwise(player.trooper);
    } else {
      reset_head_turning_speed(player.trooper);
    }
  }
  if (!player.trooper->is_frozen &&
      ((player.trooper->status == CLINGING) ||
      (player.trooper->status == GAINING_THRUST))) {
    if (SAF_buttonPressed(SAF_BUTTON_LEFT) &&
        !SAF_buttonPressed(SAF_BUTTON_RIGHT)) {
      button_pressed = 1;
      turn_counterclockwise(player.trooper);
    } else if (SAF_buttonPressed(SAF_BUTTON_RIGHT)) {
      button_pressed = 1;
      turn_clockwise(player.trooper);
    } else {
      reset_turning_speed(player.trooper);
    }
  }
  if (SAF_buttonPressed(SAF_BUTTON_B)) {
    if (
        (player.trooper->status == ATTEMPTING_TO_CLING) &&
        !--level.surrender_frames) {
      level.status = SURRENDER;
    }
    button_pressed = 1;
    if (player.trooper->status == FLYING) {
      player.trooper->status = ATTEMPTING_TO_CLING;
    } else if (
        (player.trooper->status == CLINGING) &&
        (level.status != CLEARED) &&
        !player.trooper->is_frozen) {
      player.trooper->status = GAINING_THRUST;
      player.trooper->thrust = (MIN_THRUST + MAX_THRUST) / 2;
      player.trooper->thrust_delta = THRUST_DELTA;
    } else if (!player.trooper->is_frozen &&
        (player.trooper->status == GAINING_THRUST)) {
      increase_trooper_thrust(player.trooper);
    }
  } else { /* Button B not pressed */
    level.surrender_frames = FRAMES_TO_SURRENDER; /* Reset surrender
                                                     counter */
    if (player.trooper->status == ATTEMPTING_TO_CLING) {
      player.trooper->status = FLYING;
    } else if (!player.trooper->is_frozen &&
        (player.trooper->status == GAINING_THRUST)) {
      propel_trooper(player.trooper);
    }
  }

  if (button_pressed) {
    static uint8_t fadeout_frames = SAF_FPS / 2;
    player.show_closeup = 1;
    remaining_frames = fadeout_frames;
  } else {
    if (!remaining_frames--) {
      player.show_closeup = 0;
    }
  }
}

static uint8_t
facing_to_goal_doorway(Trooper *trooper)
{
  Coord x = level.level_data->start_doorway.x1 - LO_RES(trooper->x);
  Coord y = level.level_data->start_doorway.y1 - LO_RES(trooper->y);
  Angle angle_to_doorway = my_limited_atan2(x, y);
  if (angle_distance(angle_to_doorway, trooper->bearing) > PI/16) {
    if (sum_angles(
          trooper->bearing,
          subtract_angles(
            angle_to_doorway,
            trooper->bearing)) < PI) {
      turn_counterclockwise(trooper);
    } else {
      turn_clockwise(trooper);
    }
    return 0;
  } else {
    reset_turning_speed(trooper);
    return 1;
  }
}

static uint8_t
thrust_to_max(Trooper *trooper)
{
  trooper->thrust += trooper->thrust_delta;
  if (trooper->thrust < MIN_THRUST) {
    trooper->thrust = MIN_THRUST;
    trooper->thrust_delta = -trooper->thrust_delta;
  } else if (trooper->thrust >= MAX_THRUST) {
    trooper->thrust = MAX_THRUST;
    return 1;
  }
  return 0;
}

static void
apply_trooper_action(Trooper *trooper)
{
  switch (trooper->action) {
  case ENTER_ROOM:
    advance_trooper(trooper);
    if (
        (trooper->x > 0) &&
        (trooper->x < HI_RES(63)) &&
        (trooper->y > 0) &&
        (trooper->y < HI_RES(63))) {
      let_trooper_cling(trooper);
      trooper->action =
        (trooper == player.trooper) ? PLAYER_INPUT : DART_TO_GOAL;
    };
    break;
  case DART_TO_GOAL:
    if ((level.status != LOST) &&
        !trooper->is_frozen) {

      if (trooper->status == CLINGING) {
        if (facing_to_goal_doorway(trooper)) {
          if (thrust_to_max(trooper)) {
            propel_trooper(trooper);
          }
        }
      } else { /* FLYING */
        trooper->status = ATTEMPTING_TO_CLING;
      }

      /* shoot */
      
      Coord x = LO_RES(player.trooper->x - trooper->x);
      Coord y = LO_RES(player.trooper->y - trooper->y);
      Angle angle_to_player = my_limited_atan2(x, y);
      if (
          angle_distance(
            sum_angles(
              trooper->bearing,
              trooper->look),
            angle_to_player) < HI_RES_ANGLE(16)) {
        trooper->is_shooting = 1;
      } else {
        trooper->is_shooting = 0;
      }
    }
    break;
  case PLAYER_INPUT:
    apply_user_input();
    break;
  default:
    break;
  }
}

static void
apply_troopers_actions(void)
{
  uint8_t i;
  for (i = 0; i <= level.level_data->n_opponents; i++) {
    apply_trooper_action(&level.troopers[i]);
  }
}

static void
move_trooper(Trooper *trooper)
{
  rotate_trooper(trooper);
  translate_trooper(trooper);
}

static void
check_level_status(void)
{
  if (level.status != CLEARED) {
    if (level.status != TIMED_OUT) {
      if (
          arriving_at_doorway(
            player.trooper,
            &level.level_data->goal_doorway)) {
        SAF_playSound(SAF_SOUND_BEEP);
        let_trooper_cling(player.trooper);
        level.status = CLEARED;
        score += level.remaining_frames;
        if (score > MAX_POSSIBLE_SCORE) {
          score = MAX_POSSIBLE_SCORE;
        }
        check_leaderboard_status(score);
      } else if (level.status != LOST) {
        uint8_t i;
        for (i = 1; i <= level.level_data->n_opponents; i++) {
          if (
              arriving_at_doorway(
                &level.troopers[i],
                &level.level_data->start_doorway)) {
            SAF_playSound(SAF_SOUND_BEEP);
            let_trooper_cling(&level.troopers[i]);
            level.status = LOST;
          }
        }
      }
    }
  }
}

static void
move_troopers(void)
{
  uint8_t i;
  for (i = 0; i <= level.level_data->n_opponents; i++) {
    move_trooper(&level.troopers[i]);
  }
}

static void
init_game(void)
{
  score = 0;
  scored_hi_score = 0;
  level_number = 0;
  active_loop = title_loop;
}

#if defined BUILD_LEVEL_EDITOR && defined SAF_FE_GENERIC_FRONTEND
#include "level-editor.h"
#endif

void
SAF_init(void)
{
    load_leaderboard();
#if defined BUILD_LEVEL_EDITOR && defined SAF_FE_GENERIC_FRONTEND
  if (SAF_FE_GF_parameters['e']) {
    init_editor();
    if (
        SAF_FE_GF_parameters['n'] < '0' ||
        SAF_FE_GF_parameters['n'] >= ('0' + n_levels))
      SAF_FE_GF_parameters['n'] = '0';
    level_number = SAF_FE_GF_parameters['n'] - '0';
  } else {
#endif
    init_game();
#ifdef SAF_FE_GENERIC_FRONTEND
    if (
        SAF_FE_GF_parameters['n'] < '0' ||
        SAF_FE_GF_parameters['n'] >= ('0' + n_levels))
      SAF_FE_GF_parameters['n'] = '0';
    level_number = SAF_FE_GF_parameters['n'] - '0';
#endif
#if defined BUILD_LEVEL_EDITOR && defined SAF_FE_GENERIC_FRONTEND
  }
#endif
}

uint8_t
SAF_loop(void)
{
  return active_loop();
}
