PROGRAM ?= batlroom-sdl
TESTER = test/test
FRONTEND ?= SDL2
SAF_PLATFORM = $(shell echo "SAF_PLATFORM_$(FRONTEND)" | tr a-z A-Z})
CFLAGS = -x c -g -std=c99 -O0 -fmax-errors=5 -pedantic -Wall -Wextra \
	 -D_XOPEN_SOURCE=700 -D_POSIX_C_SOURCE=200809L
LDFLAGS = -l$(FRONTEND)
BUILD_LEVEL_EDITOR = -D BUILD_LEVEL_EDITOR # comment out to omit

CC = cc
EMCC = emcc
RM = rm -f

SRCS = $(PROGRAM).c lib/saf-jsp.h Makefile saf_adapters.h drawing_code.h levels.h \
       batlroom.c level-editor.h level_data.h


$(PROGRAM): $(SRCS)
	$(CC) $(CFLAGS) $(BUILD_LEVEL_EDITOR) -D $(SAF_PLATFORM) \
	  $(EXTRA_PARAMETERS) -o $(PROGRAM) $(PROGRAM).c $(LDFLAGS)

.PHONY: check
check: $(TESTER)
	./$(TESTER)

$(TESTER): test/test.c test/test_fw.h $(SRCS)
	$(CC) -g -O0 -o $(TESTER) test/test.c $(LDFLAGS)

.PHONY: analyze
analyze:
	cppcheck --error-exitcode=1 --check-config -I /usr/include \
		-I /usr/include/x86_64-linux-gnu --language=c --std=c99 \
		--enable=all --suppress='*:lib/saf-jsp.h' --quiet -D__x86_64__ \
		-DSAF_PLATFORM_NULL batlroom-sdl.c

.PHONY: clean
clean:
	$(RM) $(PROGRAM) $(TESTER)

.PHONY: emscripten
emscripten: public/batlroom.js

public/batlroom.js:
	$(EMCC) -s USE_SDL=2 -D SAF_PLATFORM_SDL2 batlroom-sdl.c \
		-o public/batlroom.js

.PHONY: all
all: $(PROGRAM)

.PHONY: run
run: $(PROGRAM)
	./$(PROGRAM) $(RUNARGS)

# make EXTRA_PARAMETERS="-DSAF_SETTING_FORCE_1BIT=1" for monochrome version
# make EXTRA_PARAMETERS="-UBUILD_LEVEL_EDITOR" to skip editor build
