/*
    batlroom
    Copyright (C) 2021  Julio Sangrador-Paton

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifdef BUILD_LEVEL_EDITOR
static LevelData level_data[255] = {
#else
static LevelData level_data[] = {
#endif
  {
    .room_size = STANDARD_ROOM,
    .lighting = LIGHT,
    .n_opponents = 0,
    .n_stars = 0,
    .stars = { { 0 } },
    .start_doorway = { .x1 = 28, .y1 = 63, .x2 = 35, .y2 = 63,
      .orientation = NORTH },
    .goal_doorway = { .x1 = 35, .y1 = 0, .x2 = 28, .y2 = 0,
      .orientation = SOUTH },
#ifndef SMALLER_RAM
    .time = 120,
#endif
  },

  {
    .room_size = STANDARD_ROOM,
    .lighting = LIGHT,
    .n_opponents = 0,
    .n_stars = 0,
    .stars = { { 0 } },
    .start_doorway = { .x1 = 50, .y1 = 63, .x2 = 57, .y2 = 63,
      .orientation = NORTH },
    .goal_doorway = { .x1 = 15, .y1 = 0, .x2 = 8, .y2 = 0,
      .orientation = SOUTH },
#ifndef SMALLER_RAM
    .time = 120,
#endif
  },

  {
    .room_size = STANDARD_ROOM,
    .lighting = LIGHT,
    .n_opponents = 0,
    .n_stars = 0,
    .stars = { { 0 } },
    .start_doorway = { .x1 = 28, .y1 = 63, .x2 = 35, .y2 = 63,
      .orientation = NORTH },
    .goal_doorway = { .x1 = 0, .y1 = 28, .x2 = 0, .y2 = 35,
      .orientation = EAST },
#ifndef SMALLER_RAM
    .time = 120,
#endif
  },

  {
    .room_size = STANDARD_ROOM,
    .lighting = LIGHT,
    .n_opponents = 0,
    .n_stars = 0,
    .stars = { { 0 } },
    .start_doorway = { .x1 = 14, .y1 = 63, .x2 = 21, .y2 = 63,
      .orientation = NORTH },
    .goal_doorway = { .x1 = 47, .y1 = 63, .x2 = 55, .y2 = 63,
      .orientation = NORTH },
#ifndef SMALLER_RAM
    .time = 120,
#endif
  },

  {
    .room_size = STANDARD_ROOM,
    .lighting = LIGHT,
    .n_opponents = 0,
    .n_stars = 2,
    .stars = {
      {
        .x = 16,
        .y = 32,
      },
      {
        .x = 48,
        .y = 32,
      },
    },
    .start_doorway = { .x1 = 28, .y1 = 63, .x2 = 35, .y2 = 63,
      .orientation = NORTH },
    .goal_doorway = { .x1 = 35, .y1 = 0, .x2 = 27, .y2 = 0,
      .orientation = SOUTH },
#ifndef SMALLER_RAM
    .time = 120,
#endif
  },

  {
    .room_size = STANDARD_ROOM,
    .lighting = LIGHT,
    .n_opponents = 0,
    .n_stars = 2,
    .stars = {
      {
        .x = 16,
        .y = 32,
      },
      {
        .x = 48,
        .y = 32,
      },
    },
    .start_doorway = { .x1 = 0, .y1 = 29, .x2 = 0, .y2 = 37,
      .orientation = EAST },
    .goal_doorway = { .x1 = 63, .y1 = 36, .x2 = 63, .y2 = 28,
      .orientation = WEST },
#ifndef SMALLER_RAM
    .time = 120,
#endif
  },

  {
    .room_size = STANDARD_ROOM,
    .lighting = LIGHT,
    .n_opponents = 0,
    .n_stars = 4,
    .stars = {
      {
        .x = 16,
        .y = 32,
      },
      {
        .x = 48,
        .y = 32,
      },
      {
        .x = 32,
        .y = 14,
      },
      {
        .x = 32,
        .y = 48,
      },
    },
    .start_doorway = { .x1 = 15, .y1 = 63, .x2 = 23, .y2 = 63,
      .orientation = NORTH },
    .goal_doorway = { .x1 = 63, .y1 = 28, .x2 = 63, .y2 = 20,
      .orientation = WEST },
#ifndef SMALLER_RAM
    .time = 120,
#endif
  },

  {
    .room_size = STANDARD_ROOM,
    .lighting = LIGHT,
    .n_opponents = 0,
    .n_stars = 4,
    .stars = {
      {
        .x = 16,
        .y = 32,
      },
      {
        .x = 48,
        .y = 32,
      },
      {
        .x = 32,
        .y = 14,
      },
      {
        .x = 32,
        .y = 48,
      },
    },
    .start_doorway = { .x1 = 30, .y1 = 0, .x2 = 22, .y2 = 0,
      .orientation = SOUTH },
    .goal_doorway = { .x1 = 63, .y1 = 46, .x2 = 63, .y2 = 38,
      .orientation = WEST },
#ifndef SMALLER_RAM
    .time = 20,
#endif
  },

  {
    .room_size = STANDARD_ROOM,
    .lighting = LIGHT,
    .n_opponents = 1,
    .n_stars = 3,
    .stars = {
      {
        .x = 8,
        .y = 8,
      },
      {
        .x = 56,
        .y = 8,
      },
      {
        .x = 32,
        .y = 14,
      },
    },
    .start_doorway = { .x1 = 28, .y1 = 63, .x2 = 36, .y2 = 63,
      .orientation = NORTH },
    .goal_doorway = { .x1 = 36, .y1 = 0, .x2 = 28, .y2 = 0,
      .orientation = SOUTH },
#ifndef SMALLER_RAM
    .time = 90,
#endif
  },

  {
    .room_size = STANDARD_ROOM,
    .lighting = DARK,
    .n_opponents = 1,
    .n_stars = 3,
    .stars = {
      {
        .x = 8,
        .y = 8,
      },
      {
        .x = 56,
        .y = 8,
      },
      {
        .x = 32,
        .y = 14,
      },
    },
    .start_doorway = { .x1 = 28, .y1 = 63, .x2 = 36, .y2 = 63,
      .orientation = NORTH },
    .goal_doorway = { .x1 = 36, .y1 = 0, .x2 = 28, .y2 = 0,
      .orientation = SOUTH },
#ifndef SMALLER_RAM
    .time = 90,
#endif
  },

  {
    .room_size = STANDARD_ROOM,
    .lighting = LIGHT,
    .n_opponents = 1,
    .n_stars = 4,
    .stars = {
      {
        .x = 8,
        .y = 7,
      },
      {
        .x = 56,
        .y = 7,
      },
      {
        .x = 8,
        .y = 48,
      },
      {
        .x = 56,
        .y = 48,
      },
    },
    .start_doorway = { .x1 = 28, .y1 = 63, .x2 = 36, .y2 = 63,
      .orientation = NORTH },
    .goal_doorway = { .x1 = 36, .y1 = 0, .x2 = 28, .y2 = 0,
      .orientation = SOUTH },
#ifndef SMALLER_RAM
    .time = 120,
#endif
  },

  {
    .room_size = STANDARD_ROOM,
    .lighting = LIGHT,
    .n_opponents = 2,
    .n_stars = 5,
    .stars = {
      {
        .x = 8,
        .y = 7,
      },
      {
        .x = 56,
        .y = 7,
      },
      {
        .x = 8,
        .y = 48,
      },
      {
        .x = 56,
        .y = 48,
      },
      {
        .x = 19,
        .y = 30,
      },
    },
    .start_doorway = { .x1 = 0, .y1 = 26, .x2 = 0, .y2 = 34,
      .orientation = EAST },
    .goal_doorway = { .x1 = 63, .y1 = 34, .x2 = 63, .y2 = 26,
      .orientation = WEST },
#ifndef SMALLER_RAM
    .time = 150,
#endif
  },

  {
    .room_size = STANDARD_ROOM,
    .lighting = DARK,
    .n_opponents = 2,
    .n_stars = 5,
    .stars = {
      {
        .x = 8,
        .y = 7,
      },
      {
        .x = 56,
        .y = 7,
      },
      {
        .x = 8,
        .y = 48,
      },
      {
        .x = 56,
        .y = 48,
      },
      {
        .x = 19,
        .y = 30,
      },
    },
    .start_doorway = { .x1 = 0, .y1 = 26, .x2 = 0, .y2 = 34,
      .orientation = EAST },
    .goal_doorway = { .x1 = 63, .y1 = 34, .x2 = 63, .y2 = 26,
      .orientation = WEST },
#ifndef SMALLER_RAM
    .time = 150,
#endif
  },

  {
    .room_size = STANDARD_ROOM,
    .lighting = DARK,
    .n_opponents = 2,
    .n_stars = 5,
    .stars = {
      {
        .x = 8,
        .y = 7,
      },
      {
        .x = 48,
        .y = 29,
      },
      {
        .x = 32,
        .y = 48,
      },
      {
        .x = 56,
        .y = 48,
      },
      {
        .x = 19,
        .y = 30,
      },
    },
    .start_doorway = { .x1 = 0, .y1 = 40, .x2 = 0, .y2 = 48,
      .orientation = EAST },
    .goal_doorway = { .x1 = 63, .y1 = 23, .x2 = 63, .y2 = 15,
      .orientation = WEST },
#ifndef SMALLER_RAM
    .time = 150,
#endif
  },

  {
    .room_size = LARGE_ROOM,
    .lighting = LIGHT,
    .n_opponents = 6,
    .n_stars = 5,
    .stars = {
      {
        .x = 30,
        .y = 39,
      },
      {
        .x = 49,
        .y = 32,
      },
      {
        .x = 42,
        .y = 43,
      },
      {
        .x = 55,
        .y = 46,
      },
      {
        .x = 46,
        .y = 55,
      },
    },
    .start_doorway = { .x1 = 35, .y1 = 63, .x2 = 43, .y2 = 63,
      .orientation = NORTH },
    .goal_doorway = { .x1 = 63, .y1 = 41, .x2 = 63, .y2 = 33,
      .orientation = WEST },
#ifndef SMALLER_RAM
    .time = 45,
#endif
  },

};

static uint8_t n_levels = 15;
