# BATLROOM

In BATLROOM you are a space trooper participating in a zero-gravity game whose
goal is to reach the other team's doorway as soon as possible. In order to do
so you can cling to walls and propel yourself in the direction you want. You
also have a light gun that you can use to freeze your opponents. When troopers
are frozen, clinging to walls or firing the light gun is impossible for them.
You can also get frozen by an opponent.  You can open or close your arms in
order to aim the light gun.

You can play online at https://jsangradorp.gitlab.io/batlroom/ .

Good luck!

---

Batlroom was programmed using the fantastic SAF library, which allows me to
have the same code compile and run in multiple environments. I have already
built Batlroom for Linux, Arduboy, Pokitto and the web.

Visit SAF at https://gitlab.com/drummyfish/saf .

---

To buid for Pokitto I have copied and very slightly modified the Makefile from
the http://gitlab.com/drummyfish/pokittolibfree project. Thanks a lot for
that. Also, I am using the PokittoLib version that comes with
https://github.com/felipemanga/FemtoIDE . I didn't manage to compile with the
pokittolibfree version, although I would have loved to.

To build for Pokitto:

```
ln -s <PokittoLib> PokittoLib
ln -s <gcc-arm-toolchain bindir> gtc
touch My_settings.h
make -f Makefile-Pokitto
```
