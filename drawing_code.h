/*
    batlroom
    Copyright (C) 2021  Julio Sangrador-Paton

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

static void
draw_background(void)
{
  SAF_clearScreen(BACKGROUND_COLOR);
}

static void
draw_trooper(Trooper *trooper);

static void
draw_opponents(Level *level)
{
  uint8_t i;
  for (i = 1; i <= level->level_data->n_opponents; i++) {
    draw_trooper(&level->troopers[i]);
  }
}

static void
draw_walls(Level *level)
{
  uint8_t i;
  for (i = 0; i < level->n_walls; i++) {
    SAF_drawLine(
        level->walls[i].x1,
        level->walls[i].y1,
        level->walls[i].x2,
        level->walls[i].y2,
        WALL_COLOR);
  }
}

static void
draw_stars_decorations(Level *level)
{
  if (level->level_data->room_size == STANDARD_ROOM) {
    uint8_t i;
    for (i = 0; i < level->level_data->n_stars; i++) {
      SAF_drawLine(
          level->level_data->stars[i].x,
          level->level_data->stars[i].y - STAR_SIZE + 1,
          level->level_data->stars[i].x - 1,
          level->level_data->stars[i].y + STAR_SIZE,
          WALL_COLOR);
      /* SAF_drawLine(
          level->level_data->stars[i].x - 1,
          level->level_data->stars[i].y - STAR_SIZE + 1,
          level->level_data->stars[i].x,
          level->level_data->stars[i].y + STAR_SIZE,
          WALL_COLOR); */
      SAF_drawLine(
          level->level_data->stars[i].x - STAR_SIZE,
          level->level_data->stars[i].y,
          level->level_data->stars[i].x + STAR_SIZE - 1,
          level->level_data->stars[i].y + 1,
          WALL_COLOR);
      /* SAF_drawLine(
          level->level_data->stars[i].x - STAR_SIZE,
          level->level_data->stars[i].y + 1,
          level->level_data->stars[i].x + STAR_SIZE - 1,
          level->level_data->stars[i].y,
          WALL_COLOR); */
    }
  }
}

static void
draw_doorway(Doorway doorway)
{
  SAF_drawLine(
      doorway.x1,
      doorway.y1,
      doorway.x2,
      doorway.y2,
      BACKGROUND_COLOR);
}

static void
draw_doorways(Level *level)
{
  draw_doorway(level->level_data->goal_doorway);
  draw_doorway(level->level_data->start_doorway);
}

static void
draw_level(Level *level)
{
  draw_walls(level);
  draw_stars_decorations(level);
  draw_doorways(level);
  draw_opponents(level);
}

static void
draw_timer(Level * level)
{
  Color colors[4] = {
#ifdef MONOCHROME
    SAF_COLOR_BLACK,
    SAF_COLOR_BLACK,
    SAF_COLOR_BLACK,
    SAF_COLOR_BLACK
#else
    SAF_COLOR_GREEN,
    SAF_COLOR_YELLOW,
    SAF_COLOR_RED,
    SAF_COLOR_BLACK
#endif
  };
  SAF_drawLine(
      61,
      2,
      61 - level->remaining_frames * 30 / level->level_time,
      2,
      level->remaining_frames > 10 * SAF_FPS ?
      colors[0] :
      level->remaining_frames > 5 * SAF_FPS ?
      colors[1] :
      colors[2 + SAF_frame() % 2]);
}

static void
draw_player_closeup(Trooper * trooper)
{
  Color helmet_color =
    trooper == player.trooper ? PLAYER_HELMET_COLOR:OPPONENT_HELMET_COLOR;

  HiResCoord left_shoulder_x =
    CENTER_X + sine_projection(CLOSEUP_COLLARBONE_LENGTH, trooper->bearing);
  HiResCoord left_shoulder_y =
    CENTER_Y - cosine_projection(CLOSEUP_COLLARBONE_LENGTH, trooper->bearing);
  drawStrokedFilledCircle(
      left_shoulder_x,
      left_shoulder_y,
      CLOSEUP_SHOULDER_RADIUS,
      helmet_color,
      CLOSEUP_STROKE_COLOR);

  Angle left_elbow_angle =
    sum_angles(
        trooper->bearing,
        subtract_angles(negated_angle(trooper->look), PI / 4));
  HiResCoord left_elbow_x =
    left_shoulder_x + cosine_projection(
        CLOSEUP_ARM_LENGTH, left_elbow_angle);
  HiResCoord left_elbow_y =
    left_shoulder_y + sine_projection(CLOSEUP_ARM_LENGTH, left_elbow_angle);

  drawStrokedFilledCircle(
      left_elbow_x,
      left_elbow_y,
      CLOSEUP_ELBOW_RADIUS,
      helmet_color,
      CLOSEUP_STROKE_COLOR);

  HiResCoord left_hand_x =
    left_elbow_x +
    cosine_projection(
        CLOSEUP_FOREARM_LENGTH,
        subtract_angles(trooper->bearing, trooper->look));
  HiResCoord left_hand_y =
    left_elbow_y +
      sine_projection(
          CLOSEUP_FOREARM_LENGTH,
          subtract_angles(trooper->bearing, trooper->look));

  drawCircle(
    left_hand_x,
    left_hand_y,
    CLOSEUP_HAND_RADIUS,
    CLOSEUP_STROKE_COLOR,
    FILLED);

  HiResCoord right_shoulder_x =
    CENTER_X - sine_projection(CLOSEUP_COLLARBONE_LENGTH, trooper->bearing);
  HiResCoord right_shoulder_y =
    CENTER_Y + cosine_projection(CLOSEUP_COLLARBONE_LENGTH, trooper->bearing);
  drawStrokedFilledCircle(
    right_shoulder_x,
    right_shoulder_y,
    CLOSEUP_SHOULDER_RADIUS,
    helmet_color,
    CLOSEUP_STROKE_COLOR);

  Angle right_elbow_angle =
    sum_angles(trooper->bearing, sum_angles(trooper->look, PI / 4));
  HiResCoord right_elbow_x =
    right_shoulder_x + cosine_projection(
        CLOSEUP_ARM_LENGTH, right_elbow_angle);
  HiResCoord right_elbow_y =
    right_shoulder_y + sine_projection(CLOSEUP_ARM_LENGTH, right_elbow_angle);

  drawStrokedFilledCircle(
    right_elbow_x,
    right_elbow_y,
    CLOSEUP_ELBOW_RADIUS,
    helmet_color,
    CLOSEUP_STROKE_COLOR);

  HiResCoord right_hand_x =
    right_elbow_x +
      cosine_projection(
        CLOSEUP_FOREARM_LENGTH, sum_angles(trooper->bearing, trooper->look));
  HiResCoord right_hand_y =
    right_elbow_y +
      sine_projection(
        CLOSEUP_FOREARM_LENGTH, sum_angles(trooper->bearing, trooper->look));

  drawCircle(
    right_hand_x,
    right_hand_y,
    CLOSEUP_HAND_RADIUS,
    CLOSEUP_STROKE_COLOR,
    FILLED);

  /* Pistol */

#ifndef SMALLER_RAM
  drawLine(
      right_hand_x -
        cosine_projection(
          PISTOL_HALFLENGTH, sum_angles(trooper->bearing, trooper->look)),
      right_hand_y -
        sine_projection(
          PISTOL_HALFLENGTH, sum_angles(trooper->bearing, trooper->look)),
      right_hand_x +
        cosine_projection(
          PISTOL_HALFLENGTH, sum_angles(trooper->bearing, trooper->look)),
      right_hand_y +
        sine_projection(
          PISTOL_HALFLENGTH, sum_angles(trooper->bearing, trooper->look)),
      CLOSEUP_STROKE_COLOR);
#endif

  /* Head */

  drawStrokedFilledCircle(
      CENTER_X,
      CENTER_Y,
      CLOSEUP_HELMET_RADIUS,
      helmet_color,
      CLOSEUP_STROKE_COLOR);

  /* Visor */

  HiResValue visor_distance = CLOSEUP_HELMET_RADIUS - CLOSEUP_VISOR_RADIUS;
  Angle visor_angle = sum_angles(trooper->bearing, trooper->look);
  drawCircle(
    CENTER_X + cosine_projection(visor_distance, visor_angle),
    CENTER_Y + sine_projection(visor_distance, visor_angle),
    CLOSEUP_VISOR_RADIUS,
    CLOSEUP_STROKE_COLOR,
    FILLED);

#ifndef SMALLER_RAM
  /* Shine */

  drawCircle(
    CENTER_X - CLOSEUP_HELMET_RADIUS / 2,
    CENTER_Y - CLOSEUP_HELMET_RADIUS / 2,
    HI_RES(1),
    SHINE_COLOR,
    FILLED);
#endif
}

static void
draw_small_trooper(Trooper *trooper)
{
  Color helmet_color =
    trooper == player.trooper ? PLAYER_HELMET_COLOR:OPPONENT_HELMET_COLOR;

  drawCircle(
      trooper->x,
      trooper->y,
      HI_RES(1),
      helmet_color,
      FILLED);
  drawCircle(
      trooper->x,
      trooper->y,
      HI_RES(1),
      FARVIEW_STROKE_COLOR,
      NOTFILLED);
}

static void
draw_shot_trajectory(Trooper *trooper);

static void
draw_trooper(Trooper *trooper)
{
  if (level.level_data->room_size == LARGE_ROOM) {
    draw_small_trooper(trooper);
    if (trooper->is_shooting) {
      draw_shot_trajectory(trooper);
    }
    return;
  }

  Color helmet_color, stroke_color;

  helmet_color = trooper->is_hit ?
    HIT_HELMET_COLOR
    :
    (trooper->is_frozen && (trooper != player.trooper)) ?
      FROZEN_HELMET_COLOR
      :
      (trooper == player.trooper) ?
        PLAYER_HELMET_COLOR
        :
        OPPONENT_HELMET_COLOR;
  stroke_color = trooper->is_hit ?
    HIT_STROKE_COLOR
    :
    trooper->is_frozen ?
      FROZEN_STROKE_COLOR
      :
      FARVIEW_STROKE_COLOR;

  /* Shoulders */
  
  drawLine(
    trooper->x - sine_projection(FARVIEW_HELMET_RADIUS, trooper->bearing) * 2,
    trooper->y + cosine_projection(
      FARVIEW_HELMET_RADIUS, trooper->bearing) * 2,
    trooper->x + sine_projection(FARVIEW_HELMET_RADIUS, trooper->bearing) * 2,
    trooper->y - cosine_projection(
      FARVIEW_HELMET_RADIUS, trooper->bearing) * 2,
    stroke_color);

  /* Arms */

  drawLine(
    trooper->x - sine_projection(2 * FARVIEW_HELMET_RADIUS, trooper->bearing),
    trooper->y +
      cosine_projection(2 * FARVIEW_HELMET_RADIUS, trooper->bearing),
    trooper->x -
      sine_projection(2 * FARVIEW_HELMET_RADIUS, trooper->bearing) +
      cosine_projection(
        FARVIEW_HELMET_RADIUS, sum_angles(trooper->bearing, trooper->look)),
    trooper->y +
      cosine_projection(2 * FARVIEW_HELMET_RADIUS, trooper->bearing) +
      sine_projection(
        FARVIEW_HELMET_RADIUS, sum_angles(trooper->bearing, trooper->look)),
    stroke_color);
  drawLine(
    trooper->x + sine_projection(2 * FARVIEW_HELMET_RADIUS, trooper->bearing),
    trooper->y -
      cosine_projection(2 * FARVIEW_HELMET_RADIUS, trooper->bearing),
    trooper->x +
      sine_projection(2 * FARVIEW_HELMET_RADIUS, trooper->bearing) +
      cosine_projection(
        FARVIEW_HELMET_RADIUS,
        subtract_angles(trooper->bearing, trooper->look)),
    trooper->y -
      cosine_projection(2 * FARVIEW_HELMET_RADIUS, trooper->bearing) +
      sine_projection(
        FARVIEW_HELMET_RADIUS,
        subtract_angles(trooper->bearing, trooper->look)),
    stroke_color);

  /* Helmet */

  drawStrokedFilledCircle(
      trooper->x,
      trooper->y,
      FARVIEW_HELMET_RADIUS,
      helmet_color,
      stroke_color);

  /* Visor */
  Angle visor_angle = sum_angles(trooper->bearing, trooper->look);
  drawCircle(
      trooper->x + cosine_projection(FARVIEW_VISOR_RADIUS, visor_angle),
      trooper->y +   sine_projection(FARVIEW_VISOR_RADIUS, visor_angle),
      FARVIEW_VISOR_RADIUS,
      stroke_color,
      FILLED);
  if (trooper->is_shooting) {
    draw_shot_trajectory(trooper);
  }
}

static void
draw_thrust_indicator(Trooper *trooper)
{
  Color helmet_color =
    trooper == player.trooper ? PLAYER_HELMET_COLOR:OPPONENT_HELMET_COLOR;

  const Coord horizontal_offset = 2;
  uint16_t length = 32 * (trooper->thrust - MIN_THRUST) /
    (MAX_THRUST - MIN_THRUST);
  Color indicator_color = helmet_color;
  SAF_drawLine(
      horizontal_offset,
      63 - 2,
      horizontal_offset,
      63 - 2 - (Coord)length,
      indicator_color);
  SAF_drawPixel(2, (63 - 2), FARVIEW_STROKE_COLOR);
  SAF_drawPixel(2, (63 - 2 - 8), FARVIEW_STROKE_COLOR);
  SAF_drawPixel(2, (63 - 2 - 16), FARVIEW_STROKE_COLOR);
  SAF_drawPixel(2, (63 - 2 - 24), FARVIEW_STROKE_COLOR);
  SAF_drawPixel(2, (63 - 2 - 32), FARVIEW_STROKE_COLOR);
}

static void
draw_player(void)
{
  draw_trooper(player.trooper);
}

static void
draw_banner(const char *banner)
{
  if (!((SAF_frame() / 15) % 2)) {
    centerTextWithBorder(5, banner, BACKGROUND_COLOR, TEXT_COLOR, 1);
  }
}

static void
draw_surrender_countdown(void)
{
  char c[12];
  drawTextWithBorder(
      SAF_intToStr(level.surrender_frames / SAF_FPS, c),
      63 - 8,
      63 - 10,
      (SAF_frame() / (SAF_FPS / 2) % 2) ? BACKGROUND_COLOR : TEXT_COLOR,
      (SAF_frame() / (SAF_FPS / 2) % 2) ? TEXT_COLOR : BACKGROUND_COLOR,
      2);
}

static void
draw_shot_trajectory(Trooper *trooper)
{
  Color helmet_color =
    trooper == player.trooper ? PLAYER_HELMET_COLOR:OPPONENT_HELMET_COLOR;

  HiResCoord elbow_x =
    trooper->x -
    (level.level_data->room_size == LARGE_ROOM ?
     0
     :
     sine_projection(FARVIEW_HELMET_RADIUS, trooper->bearing) * 2);
  HiResCoord elbow_y =
    trooper->y +
    (level.level_data->room_size == LARGE_ROOM ?
     0
     :
     cosine_projection(FARVIEW_HELMET_RADIUS, trooper->bearing) * 2);
  SAF_drawLine(
      LO_RES(elbow_x),
      LO_RES(elbow_y),
      trooper->point_of_impact_x,
      trooper->point_of_impact_y,
      SAF_frame() & 1 ? LASER_COLOR1 : LASER_COLOR2);
  SAF_drawPixel(
      trooper->point_of_impact_x,
      trooper->point_of_impact_y,
      helmet_color);
}

static void
draw_game(void)
{
  draw_background();
  if (player.show_closeup) {
    draw_player_closeup(player.trooper);
  }
  draw_level(&level);
  if (level.status == PLAYING) {
    draw_timer(&level);
  }
  if (player.trooper->status == GAINING_THRUST) {
    draw_thrust_indicator(player.trooper);
  }
  if (
      (level.surrender_frames != FRAMES_TO_SURRENDER) &&
      (level.status != SURRENDER) &&
      (player.trooper->status == ATTEMPTING_TO_CLING)) {
    draw_surrender_countdown();
  }
  draw_player();
  if (level.status == CLEARED) {
    draw_banner("GOAL!");
  }
  if (level.status == SURRENDER) {
    draw_banner("SURRENDER");
  }
  if (level.status == LOST) {
    draw_banner("LOST");
  }
  if (level.status == TIMED_OUT) {
    draw_banner("TIMEOUT");
  }
}
