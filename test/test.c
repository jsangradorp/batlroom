#include "test_fw.h"

#define SAF_PLATFORM_NULL
#include "../batlroom.c"

TESTSUITE

TEST(sine_between_minus_127_and_plus_127)
{
  ASSERT_INTEGER_EQUALS(sine(0), 0);
  ASSERT_INTEGER_EQUALS(sine(PI / 2), 127);
  ASSERT_INTEGER_EQUALS(sine(PI), 0);
  ASSERT_INTEGER_EQUALS(sine(PI / 2 * 3), -127);
  return 1;
}
ENDTEST(sine_between_minus_127_and_plus_127)

TEST(cosine_between_minus_127_and_plus_127)
{
  ASSERT_INTEGER_EQUALS(cosine(0), 127);
  ASSERT_INTEGER_EQUALS(cosine(PI / 2), 0);
  ASSERT_INTEGER_EQUALS(cosine(PI), -127);
  ASSERT_INTEGER_EQUALS(cosine(PI / 2 * 3), 0);
  return 1;
}
ENDTEST(cosine_between_minus_127_and_plus_127)

TEST(sine_cosine_projections_inside_bounds)
{
  HiResValue magnitude = HI_RES(1);
  Angle angle;
  for (angle = 0; angle < PI / 4 * 8; angle += PI / 4) {
    ASSERT(abs(sine_projection(magnitude, angle)) <= magnitude);
    ASSERT(abs(cosine_projection(magnitude, angle)) <= magnitude);
  }
  return 1;
}
ENDTEST(sine_cosine_projections_inside_bounds)

TEST(angle_overflow_works_as_expected)
{
  Angle angle, angle_to_add;
  angle = PI / 2 * 3, angle_to_add = PI;
  Angle sum = sum_angles(angle, angle_to_add);
  ASSERT_INTEGER_EQUALS(sum, PI / 2);
  angle = PI / 2;
  Angle angle_to_subtract = PI;
  Angle difference = subtract_angles(angle, angle_to_subtract);
  ASSERT_INTEGER_EQUALS(difference, PI / 2 * 3);
  return 1;
}
ENDTEST(angle_overflow_works_as_expected)

TEST(adjust_keeps_angle_sign)
{
  Fraction f = (Fraction) {
    .numerator = 499,
    .denominator = 500,
  };
  Angle a = HI_RES_ANGLE(9);
  ASSERT_INTEGER_EQUALS(adjust_by(f, a), HI_RES_ANGLE(9));
  return 1;
}
ENDTEST(adjust_keeps_angle_sign)

ENDTESTSUITE

int
main()
{
  run_testsuite();
}
