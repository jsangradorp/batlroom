/*
    batlroom
    Copyright (C) 2021  Julio Sangrador-Paton

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

static uint8_t
level_editor_loop(void);

static void
no_action(void)
{
}

static void (*a_button_action)(void);
static void (*b_button_action)(void);

typedef enum {
  DOORWAY_EDIT,
  LEVEL_PREVIEW,
  STAR_EDIT,
  STAR_DELETE,
  LEVEL_ADD_RM
} EditMode;

static EditMode edit_mode;

typedef enum {
  CMD_SLIDER,
  CMD_CHOICE,
  CMD_COMMAND
} CommandType;

typedef void (*CommandAction)(void);

typedef struct {
  uint8_t *var;
  uint8_t min_val;
  uint8_t max_val;
  CommandAction a_button_action;
  CommandAction value_change_callback;
} SliderData;

typedef struct {
  uint8_t *var;
  uint8_t min_val;
  uint8_t max_val;
  char **option_names;
  CommandAction a_button_action;
  CommandAction value_change_callback;
} ChoiceData;

typedef union {
  SliderData slider_data;
  ChoiceData choice_data;
  CommandAction action_data;
} CommandData;

typedef struct {
  char name[11];
  CommandType type;
  CommandData data;
} Command;

static const Coord right_angle_offset = 8;
static const Coord diagonal_angle_offset = 6;

static const Coord x_offset[8] = {
  right_angle_offset,
  diagonal_angle_offset,
  0,
  -diagonal_angle_offset,
  -right_angle_offset,
  -diagonal_angle_offset,
  0,
  diagonal_angle_offset,
};

static const Coord y_offset[8] = {
  0,
  diagonal_angle_offset,
  right_angle_offset,
  diagonal_angle_offset,
  0,
  -diagonal_angle_offset,
  -right_angle_offset,
  -diagonal_angle_offset,
  };

static void
draw_coords(Coord x, Coord y)
{
  char coords_string[] = "[00,00]";
  coords_string[1] = '0' + x/10;
  coords_string[2] = '0' + x%10;
  coords_string[4] = '0' + y/10;
  coords_string[5] = '0' + y%10;
  SAF_drawText(coords_string, 2, 56, SAF_COLOR_GRAY, 1);
}

static void
draw_editor_wall(Wall *wall)
{
  SAF_drawLine(wall->x1, wall->y1, wall->x2, wall->y2, SAF_COLOR_GRAY);
  SAF_drawPixel(wall->x1, wall->y1, SAF_COLOR_GREEN);
  SAF_drawPixel(wall->x2, wall->y2, SAF_COLOR_RED);
  Coord cx = (wall->x1 + wall->x2) / 2;
  Coord cy = (wall->y1 + wall->y2) / 2;
  WallOrientation normal = (wall->orientation + 6) % (NORTHWEST + 1);
  SAF_drawLine(
      cx, cy, cx + x_offset[normal], cy + y_offset[normal], SAF_COLOR_BLUE);
}

static void
draw_editor_star(Star *star, Color color)
{
  if (level_data[level_number].room_size == STANDARD_ROOM) {
    SAF_drawRect(
        star->x-1-STAR_SIZE/2,
        star->y-STAR_SIZE/2,
        STAR_SIZE+2,
        STAR_SIZE+2,
        color,
        FILLED);
  } else {
    SAF_drawCircle(star->x, star->y, STAR_SIZE/4, SAF_COLOR_BLUE, NOTFILLED);
  }
}

static void
rotate_doorway_clockwise(Doorway *doorway)
{
  if (doorway->orientation < NORTHWEST)
    doorway->orientation += 1;
  else
    doorway->orientation = NORTH;
  doorway->x2 = doorway->x1 + x_offset[doorway->orientation];
  doorway->y2 = doorway->y1 + y_offset[doorway->orientation];
}

static const char *feedback_text;
static uint8_t providing_feedback = 0;
static uint8_t feedback_duration = 2 * SAF_FPS;

static void
give_feedback(const char *text)
{
  providing_feedback = 1;
  feedback_text = text;
  feedback_duration = 2 * SAF_FPS;
}

static void
show_feedback_window(void)
{
  SAF_drawRect(0, 10, 64, 44, SAF_COLOR_WHITE, FILLED);
  SAF_drawRect(0, 10, 64, 44, SAF_COLOR_BLACK, NOTFILLED);
  centerTextWithBorder(5, feedback_text, SAF_COLOR_WHITE, SAF_COLOR_BLACK, 1);
}

static uint8_t
len(const char *str)
{
  const char *c;
  for (c = str; *c; c++);
  return c - str;
}

static void
draw_slider_control(Command slider, uint8_t x, uint8_t y)
{
  char n[4];
  SAF_drawText(slider.name, x, y, SAF_COLOR_BLACK, 1);
  SAF_drawText(
      SAF_intToStr(*slider.data.slider_data.var, n),
      x + (len(slider.name) + 1) * 6,
      y,
      SAF_COLOR_BLACK,
      1);
}

static void
manage_slider_input(Command slider)
{
  if (
      SAF_buttonJustPressed(SAF_BUTTON_LEFT) &&
      (*slider.data.slider_data.var > slider.data.slider_data.min_val)) {
    *slider.data.slider_data.var = *slider.data.slider_data.var - 1;
    if (slider.data.slider_data.value_change_callback) {
      slider.data.slider_data.value_change_callback();
    }
  }

  if (
      SAF_buttonJustPressed(SAF_BUTTON_RIGHT) &&
      (*slider.data.slider_data.var < slider.data.slider_data.max_val)) {
    *slider.data.slider_data.var = *slider.data.slider_data.var + 1;
    if (slider.data.slider_data.value_change_callback) {
      slider.data.slider_data.value_change_callback();
    }
  }

  if (SAF_buttonJustPressed(SAF_BUTTON_A)) {
    slider.data.slider_data.a_button_action();
  }
}

static void
draw_choice_control(Command choice, uint8_t x, uint8_t y)
{
  SAF_drawText(choice.name, x, y, SAF_COLOR_BLACK, 1);
  SAF_drawText(
      choice.data.choice_data.option_names[*choice.data.choice_data.var],
      x + (len(choice.name) + 1) * 6,
      y,
      SAF_COLOR_BLACK,
      1);
}

static void
manage_choice_input(Command choice)
{
  if (
      SAF_buttonJustPressed(SAF_BUTTON_LEFT) &&
      (*choice.data.choice_data.var > choice.data.choice_data.min_val)) {
    *choice.data.choice_data.var = *choice.data.choice_data.var - 1;
    if (choice.data.choice_data.value_change_callback) {
      choice.data.choice_data.value_change_callback();
    }
  }

  if (
      SAF_buttonJustPressed(SAF_BUTTON_RIGHT) &&
      (*choice.data.choice_data.var < choice.data.choice_data.max_val)) {
    *choice.data.choice_data.var = *choice.data.choice_data.var + 1;
    if (choice.data.choice_data.value_change_callback) {
      choice.data.choice_data.value_change_callback();
    }
  }

  if (SAF_buttonJustPressed(SAF_BUTTON_A)) {
    choice.data.choice_data.a_button_action();
  }
}

static void
draw_command_control(Command command, uint8_t x, uint8_t y)
{
  SAF_drawText(command.name, x, y, SAF_COLOR_BLACK, 1);
}

static void
manage_command_input(Command command)
{
  if (SAF_buttonJustPressed(SAF_BUTTON_A)) {
    command.data.action_data();
  }
}

static void
save_levels(void)
{
  FILE *f = fopen("level_data.h", "w");
  if (!f) {
    SAF_playSound(SAF_SOUND_BUMP);
    give_feedback("Save error");
    return;
  }

  fprintf(f, "/*\n");
  fprintf(f, "    batlroom\n");
  fprintf(f, "    Copyright (C) 2021  Julio Sangrador-Paton\n");
  fprintf(f, "\n");
  fprintf(f, "    This program is free software: you can redistribute it and/or modify\n");
  fprintf(f, "    it under the terms of the GNU General Public License as published by\n");
  fprintf(f, "    the Free Software Foundation, either version 3 of the License, or\n");
  fprintf(f, "    (at your option) any later version.\n");
  fprintf(f, "\n");
  fprintf(f, "    This program is distributed in the hope that it will be useful,\n");
  fprintf(f, "    but WITHOUT ANY WARRANTY; without even the implied warranty of\n");
  fprintf(f, "    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the\n");
  fprintf(f, "    GNU General Public License for more details.\n");
  fprintf(f, "\n");
  fprintf(f, "    You should have received a copy of the GNU General Public License\n");
  fprintf(f, "    along with this program.  If not, see <https://www.gnu.org/licenses/>.\n");
  fprintf(f, "*/\n");

  fprintf(f, "#ifdef BUILD_LEVEL_EDITOR\n");
  fprintf(f, "static LevelData level_data[%d] = {\n", MAX_LEVELS);
  fprintf(f, "#else\n");
  fprintf(f, "static LevelData level_data[] = {\n");
  fprintf(f, "#endif\n");
  uint8_t i;
  for (i = 0; i < n_levels; i++) {
    fprintf(f, "  {\n");
    fprintf(f, "    .room_size = %s,\n", room_sizes[level_data[i].room_size]);
    fprintf(f, "    .lighting = %s,\n", lighting_types[level_data[i].lighting]);
    fprintf(f, "    .n_opponents = %d,\n", level_data[i].n_opponents);
    fprintf(f, "    .n_stars = %d,\n", level_data[i].n_stars);
    if (level_data[i].n_stars) {
      fprintf(f, "    .stars = {\n");
      uint8_t s;
      for (s = 0; s < level_data[i].n_stars; s++) {
        fprintf(f, "      {\n");
        fprintf(f, "        .x = %d,\n", level_data[i].stars[s].x);
        fprintf(f, "        .y = %d,\n", level_data[i].stars[s].y);
        fprintf(f, "      },\n");
      }
      fprintf(f, "    },\n");
    } else {
      fprintf(f, "    .stars = { { 0 } },\n");
    }
    fprintf(f, "    .start_doorway = { .x1 = %d, .y1 = %d, .x2 = %d, .y2 = %d,"
               "\n      .orientation = %s },\n",
        level_data[i].start_doorway.x1,
        level_data[i].start_doorway.y1,
        level_data[i].start_doorway.x2,
        level_data[i].start_doorway.y2,
        orientations[level_data[i].start_doorway.orientation]);
    fprintf(f, "    .goal_doorway = { .x1 = %d, .y1 = %d, .x2 = %d, .y2 = %d,"
               "\n      .orientation = %s },\n",
        level_data[i].goal_doorway.x1,
        level_data[i].goal_doorway.y1,
        level_data[i].goal_doorway.x2,
        level_data[i].goal_doorway.y2,
        orientations[level_data[i].goal_doorway.orientation]);
    fprintf(f, "#ifndef SMALLER_RAM");
    fprintf(f, "    .time = %d,\n", level_data[i].time);
    fprintf(f, "#endif");
    fprintf(f, "  },\n\n");
  }
  fprintf(f, "};\n\n");
  fprintf(f, "static uint8_t n_levels = %d;\n", n_levels);
  fclose(f);
  SAF_playSound(SAF_SOUND_BEEP);
  give_feedback("Save OK");
}

static void
rotate_start_doorway(void)
{
  rotate_doorway_clockwise(&level_data[level_number].start_doorway);
}

static void
rotate_goal_doorway(void)
{
  rotate_doorway_clockwise(&level_data[level_number].goal_doorway);
}

static Wall *edited_wall;

static void
start_doorway_edit(void)
{
  edit_mode = DOORWAY_EDIT;
  edited_wall = &level_data[level_number].start_doorway;
  active_loop = level_editor_loop;
  a_button_action = rotate_start_doorway;
  b_button_action = no_action;
}

static void
goal_doorway_edit(void)
{
  edit_mode = DOORWAY_EDIT;
  edited_wall = &level_data[level_number].goal_doorway;
  active_loop = level_editor_loop;
  a_button_action = rotate_goal_doorway;
  b_button_action = no_action;
}

static void
enter_level_preview(void)
{
  edit_mode = LEVEL_PREVIEW;
  active_loop = level_editor_loop;
  a_button_action = no_action;
  b_button_action = no_action;
}

static uint8_t edited_star_index;
static Star *edited_star;

static void
select_next_star(void)
{
  if (edited_star_index < (level_data[level_number].n_stars - 1)) {
    edited_star_index++;
  } else {
    edited_star_index = 0;
  }
  edited_star = &level_data[level_number].stars[edited_star_index];
}

static void
add_star(void)
{
  if (level_data[level_number].n_stars < MAX_STARS_PER_ROOM) {
    level_data[level_number].n_stars++;
    edited_star_index = level_data[level_number].n_stars - 1;
    edited_star = &level_data[level_number].stars[edited_star_index];
    edited_star->x = edited_star->y = 32;
  }
}

static void
enter_star_edit(void)
{
  edit_mode = STAR_EDIT;
  edited_star_index = 0;
  edited_star = &level_data[level_number].stars[edited_star_index];
  active_loop = level_editor_loop;
  a_button_action = select_next_star;
  b_button_action = add_star;
}

static void
delete_star(void)
{
  if (level_data[level_number].n_stars > 0) {
    uint8_t i;
    for (i = edited_star_index;
        i < (level_data[level_number].n_stars - 1); i++) {
      level_data[level_number].stars[i] =
        level_data[level_number].stars[i+1];
    }

    if (!--level_data[level_number].n_stars) {
      enter_star_edit();
    }
  }
}

static void
enter_star_delete(void)
{
  if (!level_data[level_number].n_stars) {
    enter_star_edit();
    return;
  }
  edit_mode = STAR_DELETE;
  edited_star_index = 0;
  edited_star = &level_data[level_number].stars[edited_star_index];
  active_loop = level_editor_loop;
  a_button_action = select_next_star;
  b_button_action = delete_star;
}

static void
insert_level(void)
{
  if (n_levels == MAX_LEVELS) {
    return;
  }

  uint8_t l;
  for (l = n_levels; l > level_number; l--) {
    level_data[l] = level_data[l-1];
  }
  n_levels++;
  level_number++;
}

static void
remove_level(void)
{
  if (n_levels == 1) {
    return;
  }

  uint8_t l;
  for (l = level_number; l < (n_levels - 1); l++) {
    level_data[l] = level_data[l+1];
  }
  n_levels--;
  if (level_number >= n_levels) {
    level_number = n_levels - 1;
  }
}

static void
enter_level_add_rm(void)
{
  edit_mode = LEVEL_ADD_RM;
  active_loop = level_editor_loop;
  a_button_action = insert_level;
  b_button_action = remove_level;
}

static Command commands[11];
static uint8_t n_commands;

static uint8_t editor_room_size;
static uint8_t editor_lighting;
static uint8_t editor_n_opponents;

static void
update_room_size(void)
{
  level_data[level_number].room_size = editor_room_size;
}

static void
update_lighting(void)
{
  level_data[level_number].lighting = editor_lighting;
}

static void
update_n_opponents(void)
{
  level_data[level_number].n_opponents = editor_n_opponents;
}

static void
update_editor_commands(void)
{
  editor_room_size = level_data[level_number].room_size;
  editor_lighting = level_data[level_number].lighting;
  editor_n_opponents = level_data[level_number].n_opponents;
  n_commands = sizeof(commands)/sizeof(commands[0]);
  commands[0] = (Command) {
    .name = "Level",
      .type = CMD_SLIDER,
      .data.slider_data = (SliderData) {
        .var = &level_number,
        .min_val = 0,
        .max_val = n_levels - 1,
        .a_button_action = enter_level_preview,
      }
  };
  commands[1] = (Command) {
    .name = "Lvl add/rm",
      .type = CMD_COMMAND,
      .data.action_data = enter_level_add_rm,
  };

  commands[2] = (Command) {
    .name = "Size",
      .type = CMD_CHOICE,
      .data.choice_data = (ChoiceData) {
        .var = (uint8_t *)&editor_room_size,
        .min_val = STANDARD_ROOM,
        .max_val = LARGE_ROOM,
        .option_names = room_sizes,
        .a_button_action = enter_level_preview,
        .value_change_callback = update_room_size,
      }
  };

  commands[3] = (Command) {
    .name = "Lgtn",
      .type = CMD_CHOICE,
      .data.choice_data = (ChoiceData) {
        .var = (uint8_t *)&editor_lighting,
        .min_val = DARK,
        .max_val = LIGHT,
        .option_names = lighting_types,
        .a_button_action = enter_level_preview,
        .value_change_callback = update_lighting,
      }
  };

  commands[4] = (Command) {
    .name = "Time",
      .type = CMD_SLIDER,
      .data.slider_data = (SliderData) {
        .var = (uint8_t *)&level_data[level_number].time,
        .min_val = 0,
        .max_val = 255,
        .a_button_action = enter_level_preview,
      }
  };

  commands[5] = (Command) {
    .name = "Start door",
      .type = CMD_COMMAND,
      .data.action_data = start_doorway_edit,
  };

  commands[6] = (Command) {
    .name = "Goal door",
      .type = CMD_COMMAND,
      .data.action_data = goal_doorway_edit,
  };

  commands[7] = (Command) {
    .name = "Guys",
      .type = CMD_SLIDER,
      .data.slider_data = (SliderData) {
        .var = (uint8_t *)&editor_n_opponents,
        .min_val = 0,
        .max_val = MAX_OPPONENTS,
        .a_button_action = enter_level_preview,
        .value_change_callback = update_n_opponents,
      }
  };

  commands[8] = (Command) {
    .name = "Edit stars",
      .type = CMD_COMMAND,
      .data.action_data = enter_star_edit,
  };

  commands[9] = (Command) {
    .name = "Rmve stars",
      .type = CMD_COMMAND,
      .data.action_data = enter_star_delete,
  };

  commands[10] = (Command) {
    .name = "Save",
      .type = CMD_COMMAND,
      .data.action_data = save_levels,
  };
}

static uint8_t
command_choice_loop(void)
{
  static uint8_t current_command = 0;
  SAF_clearScreen(SAF_COLOR_WHITE);
  a_button_action = no_action;
  b_button_action = no_action;
  SAF_drawText("Commands", 2, 2, SAF_COLOR_BLACK, 1);
  uint8_t row;
  for (row = 0; row < n_commands; row++) {
    switch (commands[row].type) {
    case CMD_SLIDER:
      draw_slider_control(commands[row], 2, 7 + 5 * row);
      break;
    case CMD_CHOICE:
      draw_choice_control(commands[row], 2, 7 + 5 * row);
      break;
    case CMD_COMMAND:
      draw_command_control(commands[row], 2, 7 + 5 * row);
      break;
    default:
      SAF_drawText(commands[row].name, 2, 7 + 5 * row, SAF_COLOR_BLACK, 1);
    }
  }
  SAF_drawRect(0, 5+5*current_command, 64, 8, SAF_COLOR_BLACK, 0);
  if (providing_feedback) {
    show_feedback_window();
    if (!--feedback_duration) {
      providing_feedback = 0;
    }
    return 1;
  }
  switch (commands[current_command].type) {
  case CMD_SLIDER:
    manage_slider_input(commands[current_command]);
    break;
  case CMD_CHOICE:
    manage_choice_input(commands[current_command]);
    break;
  case CMD_COMMAND:
    manage_command_input(commands[current_command]);
    break;
  default:
    break;
  }
  update_editor_commands();
  if (SAF_buttonJustPressed(SAF_BUTTON_UP) && (current_command > 0)) {
    current_command = current_command - 1;
  }
  if (
      SAF_buttonJustPressed(SAF_BUTTON_DOWN) &&
      (current_command < (n_commands - 1))) {
    current_command = current_command + 1;
  }
  return 1;
}

static void
draw_level_number(void)
{
  char n[12];
  SAF_drawText(
      SAF_intToStr(level_number, n),
      2,
      2,
      SAF_COLOR_GRAY,
      2);
}

static void
draw_editor_doorways(Level *level)
{
  Doorway *start = &level->level_data->start_doorway;
  Doorway *goal = &level->level_data->goal_doorway;
  SAF_drawLine(start->x1, start->y1, start->x2, start->y2, SAF_COLOR_GREEN);
  SAF_drawLine(goal->x1, goal->y1, goal->x2, goal->y2, SAF_COLOR_RED);
}

static void
draw_editor_opponents(Level *level)
{
  SAF_drawText("o:", 2, 20, SAF_COLOR_GRAY, 1);
  char s[12];
  SAF_drawText(
      SAF_intToStr(
        level->level_data->n_opponents, s), 14, 20, SAF_COLOR_GRAY, 1);
}

static void
draw_editor_time(Level *level)
{
  SAF_drawText("t:", 2, 14, SAF_COLOR_GRAY, 1);
  char s[12];
  SAF_drawText(
      SAF_intToStr(
        level->level_data->time, s), 14, 14, SAF_COLOR_GRAY, 1);
}

static void
draw_editor_level(Level *level)
{
  draw_level(level);
  draw_editor_doorways(level);
  draw_editor_opponents(level);
  draw_editor_time(level);
}

static uint8_t
level_editor_loop(void)
{
  static int8_t x, y;
  Coord deltax, deltay;
  initialize_level(level_number);
  switch (edit_mode) {
  case DOORWAY_EDIT:
    draw_background();
    draw_editor_level(&level);
    deltax = edited_wall->x2 - edited_wall->x1;
    deltay = edited_wall->y2 - edited_wall->y1;
    x = edited_wall->x1;
    y = edited_wall->y1;
    draw_editor_wall(edited_wall);

    if (SAF_buttonPressed(SAF_BUTTON_LEFT) && (--x < 0)) x = 0;
    if (SAF_buttonPressed(SAF_BUTTON_RIGHT) && (++x > 63)) x = 63;
    if (SAF_buttonPressed(SAF_BUTTON_UP) && (--y < 0)) y = 0;
    if (SAF_buttonPressed(SAF_BUTTON_DOWN) && (++y > 63)) y = 63;
    draw_coords(x, y);

    edited_wall->x1 = x;
    edited_wall->y1 = y;
    edited_wall->x2 = edited_wall->x1 + deltax;
    edited_wall->y2 = edited_wall->y1 + deltay;
    break;
  case LEVEL_PREVIEW:
    draw_background();
    draw_level_number();
    draw_editor_level(&level);
    if (SAF_buttonJustPressed(SAF_BUTTON_LEFT) && (level_number > 0)) {
      level_number = level_number - 1;
    }
    if (
        SAF_buttonJustPressed(SAF_BUTTON_RIGHT) &&
        (level_number < (n_levels - 1))) {
      level_number = level_number + 1;
    }
    break;
  case LEVEL_ADD_RM:
    draw_background();
    draw_level_number();
    draw_editor_level(&level);
    if (SAF_buttonJustPressed(SAF_BUTTON_LEFT) && (level_number > 0)) {
      level_number = level_number - 1;
    }
    if (
        SAF_buttonJustPressed(SAF_BUTTON_RIGHT) &&
        (level_number < (n_levels - 1))) {
      level_number = level_number + 1;
    }
    break;
  case STAR_EDIT:
    draw_background();
    if (level_data[level_number].n_stars) {
      draw_editor_star(edited_star, SAF_COLOR_BLUE);
    }
    draw_editor_level(&level);
    x = edited_star->x;
    y = edited_star->y;
    if (SAF_buttonPressed(SAF_BUTTON_LEFT) && (--x < 0)) x = 0;
    if (SAF_buttonPressed(SAF_BUTTON_RIGHT) && (++x > 63)) x = 63;
    if (SAF_buttonPressed(SAF_BUTTON_UP) && (--y < 0)) y = 0;
    if (SAF_buttonPressed(SAF_BUTTON_DOWN) && (++y > 63)) y = 63;
    draw_coords(x, y);
    edited_star->x = x;
    edited_star->y = y;
    break;
  case STAR_DELETE:
    draw_background();
    draw_editor_star(edited_star, SAF_COLOR_RED);
    draw_editor_level(&level);
    break;
  default:
    break;
  }

  if (
      SAF_buttonJustPressed(SAF_BUTTON_A) &&
      SAF_buttonJustPressed(SAF_BUTTON_B)) {
    active_loop = command_choice_loop;
    return 1;
  }

  if (SAF_buttonJustPressed(SAF_BUTTON_A)) a_button_action();
  if (SAF_buttonJustPressed(SAF_BUTTON_B)) b_button_action();
  return 1;
}

static void
init_editor(void)
{
  level_number = 0;
  update_editor_commands();
  active_loop = command_choice_loop;
  a_button_action = no_action;
}
